<?php

namespace RevPush\ServicesBundle\UrlGenerator;

interface UrlGeneratorInterface
{
    public function generate(string $path, array $parameters = []): string;
}