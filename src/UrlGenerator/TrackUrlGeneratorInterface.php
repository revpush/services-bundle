<?php

namespace RevPush\ServicesBundle\UrlGenerator;

use RevPush\ServicesBundle\PageData\OutPageData;

interface TrackUrlGeneratorInterface
{
    public function generate(OutPageData $outPageData): string;
    public function decodeRequest(string $requestUrl): OutPageData;
}