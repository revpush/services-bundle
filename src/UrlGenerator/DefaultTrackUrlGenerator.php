<?php

namespace RevPush\ServicesBundle\UrlGenerator;

use RevPush\ServicesBundle\LinkManager\LinkEncoderInterface;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\PageData\OutPageData;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

class DefaultTrackUrlGenerator implements TrackUrlGeneratorInterface
{
    private LinkEncoderInterface $linkEncoder;
    private UrlGeneratorInterface $urlGenerator;
    private UserParamsCollection $userParamsCollection;
    private string $path;

    public function __construct(
        LinkEncoderInterface $linkEncoder,
        UrlGeneratorInterface $urlGenerator,
        UserParamsCollection $userParamsCollection,
        ?string $path = 'click_out'
    )
    {
        $this->linkEncoder = $linkEncoder;
        $this->urlGenerator = $urlGenerator;
        $this->userParamsCollection = $userParamsCollection;
        $this->path = $path;
    }

    public function generate(OutPageData $outPageData): string
    {
        $adUrl = $outPageData->getAdUrl() ? $this->linkEncoder->encode($outPageData->getAdUrl()) : null;
        $outPageData->setAdUrl($adUrl);

        $params = [];
        $this->addParam($params, OutPageData::AD_URL_PARAM_NAME, $outPageData->getAdUrl());
        $this->addParam($params, OutPageData::FEED_ZONE_PARAM_NAME, $outPageData->getFeedZoneId());
        $this->addParam($params, OutPageData::AD_POSITION_PARAM_NAME, $outPageData->getAdPosition());
        $this->addParam($params, OutPageData::DISPLAY_URL_PARAM_NAME, $outPageData->getDisplayUrl());

        $this->addLinkFormatParam($params, Param::LINK_KEY);
        $this->addLinkFormatParam($params, Param::KEYWORD);
        $this->addLinkFormatParam($params, Param::KEYWORD_ID);

        return $this->urlGenerator->generate($this->path, $params);
    }

    public function decodeRequest(string $requestUrl): OutPageData
    {
        $outPageData = OutPageData::createFromUrl($requestUrl);
        if($encodedUrl = $outPageData->getAdUrl()) {
            $decodedUrl = $this->linkEncoder->decode($encodedUrl);
            $outPageData->setAdUrl($decodedUrl);
        }

        return $outPageData;
    }

    private function addParam(array &$params, string $key, $value): void
    {
        if(!is_null($value)) {
            $params[$key] = $value;
        }
    }

    private function addLinkFormatParam(array &$params, string $paramKey): void
    {
        $linkParamsCollection = $this->userParamsCollection->getLinkFormatParams();
        if ($linkParam = $linkParamsCollection->get($paramKey)) {
            $this->addParam($params, $linkParam->getUrlParamName(), $linkParam->getValue());
        }
    }
}
