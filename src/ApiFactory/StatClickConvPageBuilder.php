<?php

namespace RevPush\ServicesBundle\ApiFactory;

use RevPush\ServicesBundle\PageData\ConvPageData;
use Swagger\Client\Model\StatClickStatClickRead;

class StatClickConvPageBuilder
{
    public static function build(StatClickStatClickRead $statClick, ConvPageData $convPageData): void
    {
        if(!$statClick->getStatClickInfo()) {
            return;
        }

        if(!$statClickInfo = $statClick->getStatClickInfo()[0]) {
            return;
        }

        $statClickInfo
            ->setUserIp($convPageData->getUserIp() ?? $statClickInfo->getUserIp())
            ->setUserAgent($convPageData->getUserAgent() ?? $statClickInfo->getUserAgent())
            ->setUserReferer($convPageData->getUserReferer() ?? $statClickInfo->getUserReferer())
            ->setUserSessionId($convPageData->getSessionId() ?? $statClickInfo->getUserSessionId())
        ;

        $statClick
            ->setKeywordName($convPageData->getKeywordName() ?? $statClick->getKeywordName())
            ->setKeywordId($convPageData->getKeywordId() ?? $statClick->getKeywordId())
        ;
    }
}