<?php

namespace RevPush\ServicesBundle\ApiFactory;

use RevPush\ServicesBundle\PageData\OutPageData;
use Swagger\Client\Model\StatClickStatClickRead;

class StatClickOutPageBuilder
{
    public static function build(StatClickStatClickRead $statClick, OutPageData $outPageData): void
    {
        if(!$statClick->getStatClickInfo()) {
            return;
        }

        if(!$statClickInfo = $statClick->getStatClickInfo()[0]) {
            return;
        }

        $statClickInfo
            ->setDisplayUrl($outPageData->getDisplayUrl())
            ->setAdPosition($outPageData->getAdPosition())
            ->setFeedZoneId($outPageData->getFeedZoneId())
        ;
    }
}