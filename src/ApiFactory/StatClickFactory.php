<?php

namespace RevPush\ServicesBundle\ApiFactory;

use RevPush\ServicesBundle\ApiRepository\PartnerApiRepository;
use RevPush\ServicesBundle\ApiRepository\StatClickTypeApiRepository;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\PageData\ConvPageData;
use RevPush\ServicesBundle\PageData\OutPageData;
use RevPush\ServicesBundle\ParamDefiner\FeedPartnerDefiner;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;
use Swagger\Client\Model\StatClickInfoStatClickRead;
use Swagger\Client\Model\StatClickStatClickRead;

readonly class StatClickFactory
{
    public function __construct(
        private FeedPartnerDefiner $feedPartnerDefiner
    ) {}

    public function createFromUserParamsCollection(UserParamsCollection $userParamsCollection, int $statClickType): ?StatClickStatClickRead
    {
        $requestParams = $userParamsCollection->getRequestParams();
        $linkFormatParams = $userParamsCollection->getLinkFormatParams();
        $geoParams = $userParamsCollection->getGeoParams();

        if(is_null($linkFormatParams->getValue(Param::PARTNER_ID))) {
            return null;
        }

        $statClickInfo = new StatClickInfoStatClickRead();
        $statClickInfo
            ->setRequestUrl($requestParams->getRequestUrl())
            ->setUserIp($requestParams->getUserIp())
            ->setUserAgent($requestParams->getUserAgent())
            ->setUserReferer($requestParams->getUserReferer())
            ->setUserSessionId($requestParams->getSessionId())
            ->setVisitorId($linkFormatParams->getValue(Param::VISITOR_ID))
            ->setSourceId($linkFormatParams->getValue(Param::SOURCE_ID))
            ->setTargetId($linkFormatParams->getValue(Param::TARGET_ID))
        ;

        $statDate = new \DateTime();
        $statClick = new StatClickStatClickRead();
        $statClick
            ->setStatClickType(StatClickTypeApiRepository::getResourcePath($statClickType))
            ->setStatDate($statDate)
            ->setStatDateSimple($statDate)
            ->setStatLinkPartner(PartnerApiRepository::getResourcePath($linkFormatParams->getValue(Param::PARTNER_ID)))
            ->setAdgroupId($linkFormatParams->getValue(Param::ADGROUP_ID))
            ->setLinkKey($linkFormatParams->getValue(Param::LINK_KEY))
            ->setKeywordId($linkFormatParams->getValue(Param::KEYWORD_ID))
            ->setKeywordName($linkFormatParams->getValue(Param::KEYWORD))
            ->setMarketId($geoParams->getMarketId())
            ->setDeviceId($requestParams->getDeviceId())
            ->setStatClickInfo([$statClickInfo])
        ;

        /** @var ConvPageData $convPageData */
        if($convPageData = $userParamsCollection->getPageDataFromCollection(ConvPageData::class)) {
            StatClickConvPageBuilder::build($statClick, $convPageData);
        }

        /** @var OutPageData $outPageData */
        if($outPageData = $userParamsCollection->getPageDataFromCollection(OutPageData::class)) {
            StatClickOutPageBuilder::build($statClick, $outPageData);
        }

        if ($feedPartnerId = $this->feedPartnerDefiner->getFeedPartnerId($linkFormatParams, $statClickInfo->getFeedZoneId())) {
            $statClick->setStatPartner(PartnerApiRepository::getResourcePath($feedPartnerId));
        }

        return $statClick;
    }
}