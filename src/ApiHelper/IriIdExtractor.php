<?php

namespace RevPush\ServicesBundle\ApiHelper;

class IriIdExtractor
{
    public static function extract(string $iri): int
    {
        $fragments = explode('/', $iri);
        return (int) array_pop($fragments);
    }
}