<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class SitePageMiddleware extends Middleware
{
    private AdgroupApiRepository $adgroupApiRepository;

    public function __construct(
        AdgroupApiRepository $adgroupApiRepository
    )
    {
        $this->adgroupApiRepository = $adgroupApiRepository;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if($linkKey = $paramsCollection->getValue(Param::LINK_KEY)) {
            if ($adgroup = $this->adgroupApiRepository->findOneBy([AdgroupApiRepository::LINK_KEY_FILTER => $linkKey])) {
                $sitePagePath = null;
                if ($sitePage = $adgroup->getSitePage()) {
                    $sitePagePath = $sitePage->getPath();
                }

                if ($sitePagePath) {
                    $paramsCollection->setValue(Param::SITE_PAGE, $sitePagePath);
                }
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }
}
