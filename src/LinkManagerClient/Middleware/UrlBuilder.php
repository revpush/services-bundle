<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class UrlBuilder extends Middleware
{
    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        if ($domain = $paramsCollection->getValue(Param::SITE_DOMAIN)) {
            $parsedUrl = $this->parseUrl($linkFormat);
            $parsedUrl['host'] = $domain;

            if ($path = $paramsCollection->getValue(Param::SITE_PAGE)) {
                if (strpos($path, '/') !== 0) {
                    $path = '/' . $path;
                }
                $parsedUrl['path'] = $path;
            }

            $parsedUrlQuery = $this->parseUrlQuery($parsedUrl['query'] ?? '');
            $parsedUrlQuery = $this->appendLinkParams($paramsCollection, $parsedUrlQuery);
            if ($query = $this->buildUrlQuery($parsedUrlQuery)) {
                $parsedUrl['query'] = $query;
            }

            $linkFormat = $this->buildUrl($parsedUrl);
        }

        return parent::generate($linkFormat, $paramsCollection, $options);
    }

    private function buildUrl($parsedUrl): string
    {
        $pieces = [
            ($parsedUrl['scheme'] ?? 'https'),
            '://',
            $parsedUrl['host'] ?? '',
            $parsedUrl['path'] ?? '/',
            isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '',
            isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : ''
        ];

        return implode('', $pieces);
    }

    private function parseUrl(string $url): array
    {
        return $url ? parse_url($url) : [];
    }

    private function parseUrlQuery(string $urlQuery): array
    {
        $parsedUrlQuery = [];
        parse_str($urlQuery, $parsedUrlQuery);
        return $parsedUrlQuery ?? [];
    }

    private function buildUrlQuery(array $parsedUrlQuery): string
    {
        if (count($parsedUrlQuery) > 0) {
            return http_build_query($parsedUrlQuery);
        }

        return '';
    }

    private function appendLinkParams(ParamsCollection $paramsCollection, array $parsedUrlQuery): array
    {
        foreach ($paramsCollection->getParams() as $param) {
            if (empty($param->getUrlParamName())) {
                continue;
            }
            $parsedUrlQuery[$param->getUrlParamName()] = $param->getValue();
        }

        return $parsedUrlQuery;
    }
}