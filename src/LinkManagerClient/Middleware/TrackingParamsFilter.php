<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\Entity\Partner;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class TrackingParamsFilter extends Middleware
{
    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        $this->filterTrackingParams($paramsCollection, Partner::INMOBI, ['visitor_id']);
        return parent::generate($linkFormat, $paramsCollection, $options);
    }

    private function filterTrackingParams(ParamsCollection $paramsCollection, string $partnerGid, array $availableParams = []): void
    {
        if (!$trackingParams = $paramsCollection->getValue(Param::TRACKING_PARAMS)) {
            return;
        }

        if (!$feedPartnerGid = $paramsCollection->getValue(Param::FEED_PARTNER_GID)) {
            return;
        }

        if ($feedPartnerGid === $partnerGid) {
            parse_str((string) $trackingParams, $queryParams);
            $filteredTrackingParams = array_filter($queryParams, static function ($value, $key) use ($availableParams) {
                return in_array($key, $availableParams, true);
            }, ARRAY_FILTER_USE_BOTH);

            $filteredTrackingParamsStr = null;
            if ($filteredTrackingParams) {
                $filteredTrackingParamsStr = implode('&', array_map(static function ($value, $key) {
                    return $key . '=' . $value;
                }, $filteredTrackingParams, array_keys($filteredTrackingParams)));
            }

            $paramsCollection->setValue(Param::TRACKING_PARAMS, $filteredTrackingParamsStr);
        }
    }
}