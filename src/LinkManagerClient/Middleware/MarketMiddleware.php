<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\ApiRepository\PartnerMarketApiRepository;

class MarketMiddleware extends Middleware
{
    private AdgroupApiRepository $adgroupApiRepository;
    private PartnerMarketApiRepository $partnerMarketApiRepository;

    public function __construct(
        AdgroupApiRepository $adgroupApiRepository,
        PartnerMarketApiRepository $partnerMarketApiRepository
    )
    {
        $this->adgroupApiRepository = $adgroupApiRepository;
        $this->partnerMarketApiRepository = $partnerMarketApiRepository;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if($linkKey = $paramsCollection->getValue(Param::LINK_KEY)) {
            if ($adgroup = $this->adgroupApiRepository->findOneBy([AdgroupApiRepository::LINK_KEY_FILTER => $linkKey])) {
                $market = $adgroup->getMarket();
                $marketCode = $market->getGid();
                if ($partner = $adgroup->getSearchPartner()) {
                    $partnerMarketFilters = [
                        PartnerMarketApiRepository::MARKET_FILTER => $market->getId(),
                        PartnerMarketApiRepository::PARTNER_FILTER => $partner->getId()
                    ];
                    $partnerMarket = $this->partnerMarketApiRepository->findOneBy($partnerMarketFilters);
                    if ($partnerMarket) {
                        $marketCode = $partnerMarket->getMarketCode();
                    }
                }

                $paramsCollection->setValue(Param::MARKET_GID, $marketCode);
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }
}
