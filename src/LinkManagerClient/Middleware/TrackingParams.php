<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class TrackingParams extends Middleware
{
    const ADD_TRACKING_PARAMS = false;

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        parent::parse($url, $paramsCollection, $options);
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        $link = parent::generate($linkFormat, $paramsCollection, $options);

        if(isset($options[self::ADD_TRACKING_PARAMS]) && $options[self::ADD_TRACKING_PARAMS]) {
            $additionalParams = [];
            if($trackingParams = $paramsCollection->getValue(Param::TRACKING_PARAMS)) {
                $additionalParams[] = $trackingParams;
            }

            /** @var Param $linkKeyParam */
            if($linkKeyParam = $paramsCollection->get(Param::LINK_KEY)) {
                $additionalParams[] = $linkKeyParam->getUrlParamName() . '=' . $linkKeyParam->getValue();
            }

            if (count($additionalParams) > 0) {
                $link .= (parse_url($link, PHP_URL_QUERY) ? '&' : '?') . implode('&', $additionalParams);
            }
        }

        return $link;
    }
}