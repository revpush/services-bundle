<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\LinkManagerClient\Exception\PartnerChannelEntityNotFoundException;
use RevPush\ServicesBundle\LinkManagerClient\Exception\PartnerChannelNotFoundException;
use RevPush\ServicesBundle\PartnerChannel\PartnerChannelCriteriaDto;
use RevPush\ServicesBundle\PartnerChannel\PartnerChannelDto;
use RevPush\ServicesBundle\PartnerChannel\PartnerChannelRepositoryInterface;
use Swagger\Client\Model\AdgroupAdgroupRead;
use Swagger\Client\Model\KeywordKeywordRead;
use Swagger\Client\Model\PartnerPartnerRead;

class PartnerChannel extends Middleware
{
    public const OPTION_GET_OR_FILL_CHANNEL = 'partner_channel_get_or_fill';

    public function __construct(
        private PartnerChannelRepositoryInterface $partnerChannelRepository,
        private AdgroupApiRepository $adgroupApiRepository,
    )
    {
        $this->partnerChannelRepository = $partnerChannelRepository;
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        $partnerChannel = null;

        if (!$paramsCollection->getValue(Param::PARTNER_CHANNEL_ID)) {
            $searchPartnerId = (int)$paramsCollection->getValue(Param::FEED_PARTNER_ID);
            $adgroupId = (int)$paramsCollection->getValue(Param::ADGROUP_ID);
            $keywordId = (int)$paramsCollection->getValue(Param::KEYWORD_ID);

            if ($searchPartnerId && $adgroupId && $this->partnerChannelRepository->isPartnerUsesChannels($searchPartnerId)) {
                $partnerChannelCriteria = new PartnerChannelCriteriaDto($searchPartnerId, $adgroupId, $keywordId);
                
                if ($options[self::OPTION_GET_OR_FILL_CHANNEL] ?? null) {
                    $partnerChannel = $this->partnerChannelRepository->findOneOrFillByCriteria($partnerChannelCriteria);
                } else {
                    $partnerChannel = $this->partnerChannelRepository->findOneByCriteria($partnerChannelCriteria);
                }

                if ($partnerChannel) {
                    $paramsCollection->setValue(Param::PARTNER_CHANNEL, $partnerChannel->getChannel());
                    $paramsCollection->setValue(Param::PARTNER_CHANNEL_ID, $partnerChannel->getId());
                } else {
                    throw new PartnerChannelNotFoundException($adgroupId, $searchPartnerId);
                }
            }
        }

        if ($partnerChannel === null && $partnerChannelId = (int)$paramsCollection->getValue(Param::PARTNER_CHANNEL_ID)) {
            if ($partnerChannel = $this->partnerChannelRepository->findOneById($partnerChannelId)) {
                $paramsCollection->setValue(Param::PARTNER_CHANNEL, $partnerChannel->getChannel());
            } else {
                throw new PartnerChannelEntityNotFoundException($partnerChannelId);
            }
        }

        if ($partnerChannel) {
            $this->addPartnerChannelParams($partnerChannel, $paramsCollection);
        }

        return parent::generate($linkFormat, $paramsCollection, $options);
    }
    
    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        /** @var AdgroupAdgroupRead $adgroup */
        $adgroup = null;
        if ($adgroupId = $paramsCollection->getValue(Param::ADGROUP_ID)) {
            $adgroup = $this->adgroupApiRepository->find($adgroupId);
        }

        if ($adgroup) {
            /** @var KeywordKeywordRead $keyword */
            $keywordId = $paramsCollection->getValue(Param::KEYWORD_ID);
            if (!$keywordId) {
                /** @var KeywordKeywordRead $keyword */
                foreach ($adgroup->getKeywords() as $keyword) {
                    if ($keyword->getIsPrimary()) {
                        $keywordId = $keyword->getId();
                    }
                }
            }

            /** @var PartnerPartnerRead $searchPartner */
            $searchPartner = $adgroup?->getSearchPartner();
            if ($searchPartner && $searchPartner->getUseChannels()) {
                $channelDto = $this->partnerChannelRepository->findOneByCriteria(
                    new PartnerChannelCriteriaDto(
                        $searchPartner->getId(), $adgroupId, (int)$keywordId
                    )
                );
                if ($channelDto) {
                    $paramsCollection->setValue(Param::PARTNER_CHANNEL, $channelDto->getChannel());
                    $paramsCollection->setValue(Param::PARTNER_CHANNEL_ID, $channelDto->getId());
                    $this->addPartnerChannelParams($channelDto, $paramsCollection);
                }
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }

    private function addPartnerChannelParams(PartnerChannelDto $partnerChannel, ParamsCollection $paramsCollection): void
    {
        foreach ($partnerChannel->getParams() as $partnerChannelParam) {
            $param = new Param;
            $param->gid = sprintf('%s_%s', Param::PARTNER_CHANNEL, $partnerChannelParam->getName());
            $param->name = sprintf('{%s.%s}', Param::PARTNER_CHANNEL, $partnerChannelParam->getName());
            $param->value = $partnerChannelParam->getValue();
            $paramsCollection->add($param);
        }
    }
}