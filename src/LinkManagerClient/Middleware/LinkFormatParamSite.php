<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\ApiRepository\LinkFormatParamSiteApiRepository;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use Swagger\Client\Model\LinkFormatParamSiteLinkFormatParamSiteRead;

class LinkFormatParamSite extends Middleware
{
    private LinkFormatParamSiteApiRepository $linkFormatParamSiteApiRepository;

    public function __construct(LinkFormatParamSiteApiRepository $linkFormatParamSiteApiRepository)
    {
        $this->linkFormatParamSiteApiRepository = $linkFormatParamSiteApiRepository;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        $this->loadLinkFormatParamSite($paramsCollection);

        parent::parse($url, $paramsCollection, $options);
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        $this->loadLinkFormatParamSite($paramsCollection);

        return parent::generate($linkFormat, $paramsCollection, $options);
    }

    private function loadLinkFormatParamSite(ParamsCollection $paramsCollection): void
    {
        if ($siteId = $paramsCollection->getValue(Param::SITE_ID)) {
            $collection = $this->linkFormatParamSiteApiRepository->findBy([LinkFormatParamSiteApiRepository::SITE_ID_FILTER => $siteId]);
            /** @var LinkFormatParamSiteLinkFormatParamSiteRead $item */
            foreach ($collection as $item) {
                if ($param = $paramsCollection->get($item->getLinkFormatParam()->getGid())) {
                    $param->setUrlParamName($item->getUrlParamName());
                }
            }
        }
    }
}