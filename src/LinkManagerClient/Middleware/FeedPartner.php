<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use Override;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\ParamDefiner\FeedPartnerDefiner;

class FeedPartner extends Middleware
{
    public function __construct(
        private readonly FeedPartnerDefiner $feedPartnerDefiner
    ) {}

    #[Override]
    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if (!$paramsCollection->getValue(Param::FEED_PARTNER_ID)) {
            if ($feedPartnerId = $this->feedPartnerDefiner->getFeedPartnerId($paramsCollection)) {
                $paramsCollection->setValue(Param::FEED_PARTNER_ID, $feedPartnerId);
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }
}
