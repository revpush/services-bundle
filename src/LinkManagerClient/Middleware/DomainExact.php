<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class DomainExact extends Middleware
{
    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if($domainParam = $paramsCollection->get(Param::SITE_DOMAIN)) {
            if($domain = parse_url($url, PHP_URL_HOST)) {
                $domainParam->setValue($domain);
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }
}