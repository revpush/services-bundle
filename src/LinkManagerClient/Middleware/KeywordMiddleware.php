<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\KeywordRotation\KeywordRotationService;
use RevPush\ServicesBundle\KeywordRotation\Keyword as KeywordRotationKeyword;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use Swagger\Client\Model\AdgroupAdgroupRead;
use Swagger\Client\Model\KeywordKeywordRead;

class KeywordMiddleware extends Middleware
{
    public const string OPTION_NEED_KEYWORD_ROTATION = 'need_keyword_rotation';

    public function __construct(
        protected readonly AdgroupApiRepository $adgroupApiRepository,
        private readonly KeywordRotationService $keywordRotationService
    ) {}

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        $keywordName = $paramsCollection->getValue(Param::KEYWORD);
        $keywordId = $paramsCollection->getValue(Param::KEYWORD_ID);

        $keyword = null;
        if ($adgroup = $this->adgroupApiRepository->findByLinkKey($paramsCollection->getValue(Param::LINK_KEY))) {
            if ($options[self::OPTION_NEED_KEYWORD_ROTATION] ?? null) {
                $keyword = $this->getKeywordByRotation($adgroup);
            } else if ($this->hasKeywordIdParamInUrl($paramsCollection, $url)) {
                $urlKeywordId = $this->getKeywordIdValueFromUrl($paramsCollection, $url);
                if (!is_numeric($urlKeywordId)) {
                    $keywordId = null;
                } else {
                    $keywordId = $this->getKeywordById($urlKeywordId, $adgroup)?->getId();
                }
            } else if ($keywordName) {
                $keyword = $this->getKeywordByWord($keywordName, $adgroup);
            }
        }

        if ($keyword) {
            $paramsCollection->setValue(Param::KEYWORD, $keyword->getName());
            $paramsCollection->setValue(Param::KEYWORD_REPLACEMENT, $keyword->getName());
            $paramsCollection->setValue(Param::KEYWORD_ID, $keyword->getId());
        } elseif ($keywordName) {
            $paramsCollection->setValue(Param::KEYWORD, $keywordName);
            $paramsCollection->setValue(Param::KEYWORD_REPLACEMENT, $keywordName);
            $paramsCollection->setValue(Param::KEYWORD_ID, $keywordId);
        }

        parent::parse($url, $paramsCollection, $options);
    }

    private function hasKeywordIdParamInUrl(ParamsCollection $paramsCollection, string $url): bool
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $queryParams);
        if (!$keywordIdParam = $paramsCollection->get(Param::KEYWORD_ID)) {
            return false;
        }

        return isset($queryParams[$keywordIdParam->getUrlParamName()]);
    }

    private function getKeywordIdValueFromUrl(ParamsCollection $paramsCollection, string $url): ?string
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $queryParams);
        if (!$keywordIdParam = $paramsCollection->get(Param::KEYWORD_ID)) {
            return false;
        }

        return $queryParams[$keywordIdParam->getUrlParamName()] ?? null;
    }

    public function getKeywordByWord(string $keyword, AdgroupAdgroupRead $adgroup): ?KeywordRotationKeyword
    {
        /** @var KeywordKeywordRead $keywordObject */
        foreach ($adgroup->getKeywords() as $keywordObject) {
            if ($keyword === $keywordObject->getName()) {
                return KeywordRotationKeyword::createFromApiEntity($keywordObject);
            }
        }

        return null;
    }

    public function getKeywordById(int $keywordId, AdgroupAdgroupRead $adgroup): ?KeywordRotationKeyword
    {
        /** @var KeywordKeywordRead $keywordObject */
        foreach ($adgroup->getKeywords() as $keywordObject) {
            if ($keywordId === $keywordObject->getId()) {
                return KeywordRotationKeyword::createFromApiEntity($keywordObject);
            }
        }

        return null;
    }

    public function getKeywordByRotation(AdgroupAdgroupRead $adgroup): ?KeywordRotationKeyword
    {
        $keywords = $adgroup->getKeywords();
        if (count($keywords) > 0) {
            $keywordsForRotation = [];
            foreach ($keywords as $keyword) {
                $keywordsForRotation[] = KeywordRotationKeyword::createFromApiEntity($keyword);
            }

            return $this->keywordRotationService->getKeywordRotation($keywordsForRotation);
        }

        return null;
    }
}
