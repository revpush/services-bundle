<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\ApiRepository\SiteApiRepository;
use RevPush\ServicesBundle\LinkManagerClient\Middleware\AbstractDomainPrefix;
use Swagger\Client\Model\SiteSiteRead;

class DomainPrefix extends AbstractDomainPrefix
{
    public function __construct(
        private readonly SiteApiRepository $siteApiRepository,
    )
    {
    }

    public function isSiteUseWww(int $sitId): bool
    {
        $site = $this->siteApiRepository->find($sitId);
        if ($site instanceof SiteSiteRead) {
            return $site->getUseWww();
        }

        return false;
    }
}