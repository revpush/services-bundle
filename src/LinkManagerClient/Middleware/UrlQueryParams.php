<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class UrlQueryParams extends Middleware
{
    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        parse_str(parse_url($url, PHP_URL_QUERY) ?? '', $queryParams);
        foreach($paramsCollection->getParams() as $param) {
            if(isset($queryParams[$param->getUrlParamName()])) {
                if($value = $queryParams[$param->getUrlParamName()]) {
                    $paramsCollection->setValue($param->getGid(), $value);
                }
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }
}