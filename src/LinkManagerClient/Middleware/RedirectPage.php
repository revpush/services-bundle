<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\LinkManager\LinkEncoderInterface;
use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

class RedirectPage extends Middleware
{
    private LinkEncoderInterface $linkEncoder;

    public function __construct(
        LinkEncoderInterface $linkEncoder
    )
    {
        $this->linkEncoder = $linkEncoder;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if($redirectPage = $paramsCollection->getValue(Param::REDIRECT_PAGE)) {
            $decodedValue = $this->linkEncoder->decode($redirectPage);
            $paramsCollection->setValue(Param::REDIRECT_PAGE, $decodedValue);
        }

        parent::parse($url, $paramsCollection, $options);
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        if($redirectPage = $paramsCollection->getValue(Param::REDIRECT_PAGE)) {
            $decodedValue = $this->linkEncoder->encode($redirectPage);
            $paramsCollection->setValue(Param::REDIRECT_PAGE, $decodedValue);
        }

        return parent::generate($linkFormat, $paramsCollection, $options);
    }
}