<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;

use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

abstract class AbstractDomainPrefix extends Middleware
{
    abstract public function isSiteUseWww(int $sitId): bool;

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        $siteId = $paramsCollection->getValue(Param::SITE_ID);
        $domainParam = $paramsCollection->get(Param::SITE_DOMAIN);
        if ($siteId && $this->isSiteUseWww($siteId) && $domainParam) {
            $linkFormat = str_replace($domainParam->getName(), 'www.' . $domainParam->getName(), $linkFormat);
        }

        return parent::generate($linkFormat, $paramsCollection, $options);
    }
}