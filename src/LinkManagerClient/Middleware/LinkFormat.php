<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;

class LinkFormat extends Middleware
{
    private AdgroupApiRepository $adgroupApiRepository;

    public function __construct(
        AdgroupApiRepository $adgroupApiRepository
    )
    {
        $this->adgroupApiRepository = $adgroupApiRepository;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if($linkKey = $paramsCollection->getValue(Param::LINK_KEY)) {
            if($adgroup = $this->adgroupApiRepository->findOneBy([AdgroupApiRepository::LINK_KEY_FILTER => $linkKey])) {
                if($linkFormat = $adgroup->getFinalTargetUrl()) {
                    $linkFormatEscaped = preg_quote($linkFormat, '/');

                    $search = [];
                    $replace = [];

                    /**
                     * @var Param $param
                     */
                    foreach($paramsCollection->getParams() as $key => $param) {
                        $search[$key] = preg_quote($param->getName(), '/');
                        $replace[$key] = "(?P<{$param->getGid()}>[^\/\&\?]*)";
                    }

                    $pregReplacePattern = "/" . str_replace($search, $replace, $linkFormatEscaped) . "/";
                    preg_match($pregReplacePattern, $url, $matches);
                    foreach($matches as $key => $value) {
                        if(!is_int($key) && $paramsCollection->isset($key)) {
                            $paramsCollection->setValue($key, $value);
                        }
                    }
                }
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        return parent::generate($linkFormat, $paramsCollection, $options);
    }
}