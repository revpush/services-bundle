<?php


namespace RevPush\ServicesBundle\LinkManagerClient\Middleware;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;


class LinkKey extends Middleware
{
    private AdgroupApiRepository $adgroupApiRepository;

    public function __construct(
        AdgroupApiRepository $adgroupApiRepository
    )
    {
        $this->adgroupApiRepository = $adgroupApiRepository;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if($linkKey = $paramsCollection->getValue(Param::LINK_KEY)) {
            if($adgroup = $this->adgroupApiRepository->findOneBy([AdgroupApiRepository::LINK_KEY_FILTER => $linkKey])) {
                $paramsCollection->setValue(Param::SEARCH_CAMPAIGN_ID, $adgroup->getSearchCampaignId());
                $paramsCollection->setValue(Param::ADGROUP_SYSTEM_NAME, $adgroup->getSystemName());
                $paramsCollection->setValue(Param::ADGROUP_ID, $adgroup->getId());
                $paramsCollection->setValue(Param::PARTNER_SHORT_NAME, $adgroup->getPartner()->getShortName());
                $paramsCollection->setValue(Param::PARTNER_ID, $adgroup->getPartner()->getId());
                $paramsCollection->setValue(Param::PARTNER_GID, $adgroup->getPartner()->getGid());
                $paramsCollection->setValue(Param::MARKET, $adgroup->getMarket()->getName());
                $paramsCollection->setValue(Param::MARKET_GID, $adgroup->getMarket()->getGid());
                $paramsCollection->setValue(Param::MARKET_ID, $adgroup->getMarket()->getId());
                $paramsCollection->setValue(Param::USER_TEAM_ID, $adgroup->getUserTeam()->getId());
                $paramsCollection->setValue(Param::USER_TEAM_NAME, $adgroup->getUserTeam()->getName());
                $paramsCollection->setValue(Param::SITE_ID, $adgroup->getSite()->getId());
                $paramsCollection->setValue(Param::SITE_SHORT_NAME, $adgroup->getSite()->getShortName());
            }
        }

        parent::parse($url, $paramsCollection, $options);
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        if($linkKey = $paramsCollection->getValue(Param::LINK_KEY)) {
            if($adgroup = $this->adgroupApiRepository->findOneBy([AdgroupApiRepository::LINK_KEY_FILTER => $linkKey])) {
                $paramsCollection->setValue(Param::SITE_DOMAIN, $adgroup->getSite()->getDomain());
                $paramsCollection->setValue(Param::SEARCH_CAMPAIGN_ID, $adgroup->getSearchCampaignId());
                $paramsCollection->setValue(Param::ADGROUP_SYSTEM_NAME, $adgroup->getSystemName());
                $paramsCollection->setValue(Param::PARTNER_SHORT_NAME, $adgroup->getPartner()->getShortName());
                $paramsCollection->setValue(Param::MARKET, $adgroup->getMarket()->getName());
                $paramsCollection->setValue(Param::MARKET_GID, $adgroup->getMarket()->getGid());
            }
        }

        return parent::generate($linkFormat, $paramsCollection, $options);
    }
}