<?php


namespace RevPush\ServicesBundle\LinkManagerClient;

use RevPush\ServicesBundle\ApiRepository\LinkFormatParamApiRepository;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\LinkManager\ParamsCollectionFactoryInterface;
use Swagger\Client\Model\LinkFormatParamLinkFormatParamRead;

class ParamsCollectionFactory implements ParamsCollectionFactoryInterface
{
    /**
     * @var LinkFormatParamApiRepository
     */
    private LinkFormatParamApiRepository $apiRepository;

    public function __construct(
        LinkFormatParamApiRepository $apiRepository
    )
    {
        $this->apiRepository = $apiRepository;
    }

    public function createCollection(): ParamsCollection
    {
        $paramsCollection = new ParamsCollection();
        $collection = $this->apiRepository->findAll();
        /** @var LinkFormatParamLinkFormatParamRead $item */
        foreach($collection as $item) {
            $param = new Param();

            $param->id = $item->getId();
            $param->name = $item->getName();
            $param->gid = $item->getGid();
            $param->urlParamName = $item->getUrlParamName();
            $param->needEncode = $item->getNeedEncode();

            $paramsCollection->add($param);
        }

        return $paramsCollection;
    }
}