<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Exception;

class PartnerChannelEntityNotFoundException extends \Exception
{
    public function __construct(int $partnerChannelId)
    {
        $message = sprintf('Could not find partner channel by ID: "%d"', $partnerChannelId);
        parent::__construct($message);
    }
}