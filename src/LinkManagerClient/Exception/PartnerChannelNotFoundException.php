<?php

namespace RevPush\ServicesBundle\LinkManagerClient\Exception;

class PartnerChannelNotFoundException extends \Exception
{
    public function __construct(int $adgroupId, int $partnerId)
    {
        $message = sprintf('Could not find actual partner channel for adgroup ID: "%d" and partner ID: "%d"', $adgroupId, $partnerId);
        parent::__construct($message);
    }
}