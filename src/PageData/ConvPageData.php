<?php

namespace RevPush\ServicesBundle\PageData;

class ConvPageData implements PageDataParamsInterface
{
    public const USER_IP = 'user_ip';
    public const USER_AGENT = 'user_agent';
    public const USER_REFERER = 'user_referer';
    public const SESSION_ID = 'session_id';
    public const KEYWORD_ID = 'keyword_id';
    public const KEYWORD_NAME = 'keyword';

    private ?string $userIp = null;
    private ?string $userAgent = null;
    private ?string $userReferer = null;
    private ?string $sessionId = null;
    private ?int $keywordId = null;
    private ?string $keywordName = null;

    public static function createFromUrl(string $requestUrl): ConvPageData
    {
        $query = parse_url($requestUrl, PHP_URL_QUERY);
        return ConvPageData::createFromData($query);
    }

    public static function createFromData(array $data = []): ConvPageData
    {
        $keywordId = isset($data[self::KEYWORD_ID]) ? (int) $data[self::KEYWORD_ID] : null;

        return (new ConvPageData())
            ->setUserIp($data[self::USER_IP] ?? null)
            ->setUserAgent($data[self::USER_AGENT] ?? null)
            ->setUserReferer($data[self::USER_REFERER] ?? null)
            ->setSessionId($data[self::SESSION_ID] ?? null)
            ->setKeywordId($keywordId)
            ->setKeywordName($data[self::KEYWORD_NAME] ?? null)
        ;
    }

    /**
     * @return string|null
     */
    public function getUserIp(): ?string
    {
        return $this->userIp;
    }

    /**
     * @param string|null $userIp
     * @return ConvPageData
     */
    public function setUserIp(?string $userIp): ConvPageData
    {
        $this->userIp = $userIp;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @param string|null $userAgent
     * @return ConvPageData
     */
    public function setUserAgent(?string $userAgent): ConvPageData
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserReferer(): ?string
    {
        return $this->userReferer;
    }

    /**
     * @param string|null $userReferer
     * @return ConvPageData
     */
    public function setUserReferer(?string $userReferer): ConvPageData
    {
        $this->userReferer = $userReferer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    /**
     * @param string|null $sessionId
     * @return ConvPageData
     */
    public function setSessionId(?string $sessionId): ConvPageData
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getKeywordId(): ?int
    {
        return $this->keywordId;
    }

    /**
     * @param int|null $keywordId
     * @return ConvPageData
     */
    public function setKeywordId(?int $keywordId): ConvPageData
    {
        $this->keywordId = $keywordId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeywordName(): ?string
    {
        return $this->keywordName;
    }

    /**
     * @param string|null $keywordName
     * @return ConvPageData
     */
    public function setKeywordName(?string $keywordName): ConvPageData
    {
        $this->keywordName = $keywordName;
        return $this;
    }
}