<?php

namespace RevPush\ServicesBundle\PageData;

class OutPageData implements PageDataParamsInterface
{
    public const AD_URL_PARAM_NAME = 'page';
    public const FEED_ZONE_PARAM_NAME = 'feed_zone';
    public const AD_POSITION_PARAM_NAME = 'ad_position';
    public const DISPLAY_URL_PARAM_NAME = 'display_url';

    private ?string $adUrl = null;
    private ?int $feedZoneId = null;
    private ?int $adPosition = null;
    private ?string $displayUrl = null;

    public static function createFromParams(
        ?string $page = null,
        ?int $feedZone = null,
        ?int $adPosition = null,
        ?string $displayUrl = null
    ): OutPageData
    {
        return (new OutPageData())
            ->setAdUrl($page)
            ->setFeedZoneId($feedZone)
            ->setAdPosition($adPosition)
            ->setDisplayUrl($displayUrl)
        ;
    }

    public static function createFromUrl(string $requestUrl): OutPageData
    {
        $queryString = parse_url($requestUrl, PHP_URL_QUERY);
        parse_str($queryString, $queryParams);
        return OutPageData::createFromData($queryParams);
    }

    public static function createFromData(array $data = []): OutPageData
    {
        $adUrl = isset($data[self::AD_URL_PARAM_NAME]) ? (string) $data[self::AD_URL_PARAM_NAME] : null;
        $feedZoneId = isset($data[self::FEED_ZONE_PARAM_NAME]) ? (int) $data[self::FEED_ZONE_PARAM_NAME] : null;
        $adPosition = isset($data[self::AD_POSITION_PARAM_NAME]) ? (int) $data[self::AD_POSITION_PARAM_NAME] : null;
        $displayUrl = isset($data[self::DISPLAY_URL_PARAM_NAME]) ? (string) $data[self::DISPLAY_URL_PARAM_NAME] : null;

        return (new OutPageData())
            ->setAdUrl($adUrl)
            ->setFeedZoneId($feedZoneId)
            ->setAdPosition($adPosition)
            ->setDisplayUrl($displayUrl)
        ;
    }

    /**
     * @return string|null
     */
    public function getAdUrl(): ?string
    {
        return $this->adUrl;
    }

    /**
     * @param string|null $adUrl
     * @return OutPageData
     */
    public function setAdUrl(?string $adUrl): OutPageData
    {
        $this->adUrl = $adUrl;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFeedZoneId(): ?int
    {
        return $this->feedZoneId;
    }

    /**
     * @param int|null $feedZoneId
     * @return OutPageData
     */
    public function setFeedZoneId(?int $feedZoneId): OutPageData
    {
        $this->feedZoneId = $feedZoneId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAdPosition(): ?int
    {
        return $this->adPosition;
    }

    /**
     * @param int|null $adPosition
     * @return OutPageData
     */
    public function setAdPosition(?int $adPosition): OutPageData
    {
        $this->adPosition = $adPosition;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDisplayUrl(): ?string
    {
        return $this->displayUrl;
    }

    /**
     * @param string|null $displayUrl
     * @return OutPageData
     */
    public function setDisplayUrl(?string $displayUrl): OutPageData
    {
        $this->displayUrl = $displayUrl;
        return $this;
    }
}