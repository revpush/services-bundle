<?php


namespace RevPush\ServicesBundle\LinkManager\Middleware;


abstract class MiddlewareFactory
{
    private MiddlewareCollection $collection;

    public function __construct(MiddlewareCollection $collection)
    {
        $this->collection = $collection;
    }

    protected function build(array $list): Middleware
    {
        if(count($list) === 0) {
            throw new \Exception('List of middlewares is empty');
        }

        $previousMiddleware = null;
        $firstMiddleware = null;
        foreach($list as $key) {
            if($middleware = $this->collection->get($key)) {
                $clonedMiddleware = clone $middleware;
                if(is_null($firstMiddleware)) {
                    $firstMiddleware = $clonedMiddleware;
                } else {
                    $previousMiddleware->setNext($clonedMiddleware);
                }

                $previousMiddleware = $clonedMiddleware;
            }
        }

        return $firstMiddleware;
    }
}