<?php


namespace RevPush\ServicesBundle\LinkManager\Middleware;


interface MiddlewareFactoryInterface
{
    public function createMiddleware(?array $list = null): Middleware;
}