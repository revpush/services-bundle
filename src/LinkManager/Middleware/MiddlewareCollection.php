<?php


namespace RevPush\ServicesBundle\LinkManager\Middleware;


class MiddlewareCollection
{
    private array $middlewares = [];

    public function get(string $key): ?Middleware
    {
        return $this->middlewares[$key] ?? null;
    }

    public function set(Middleware $middleware)
    {
        $this->middlewares[$middleware->getKey()] = $middleware;
    }
}