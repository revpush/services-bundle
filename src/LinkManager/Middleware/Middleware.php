<?php


namespace RevPush\ServicesBundle\LinkManager\Middleware;


use RevPush\ServicesBundle\LinkManager\LinkGenerator\LinkGeneratorInterface;
use RevPush\ServicesBundle\LinkManager\LinkParser\LinkParserInterface;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;

abstract class Middleware implements LinkParserInterface, LinkGeneratorInterface, MiddlewareInterface
{
    private ?Middleware $next = null;

    public function getKey()
    {
        return get_class($this);
    }

    public function setNext(Middleware $middleware): Middleware
    {
        $this->next = $middleware;

        return $this->next;
    }

    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void
    {
        if(!$this->next) {
            return;
        }

        $this->next->parse($url, $paramsCollection, $options);
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        if(!$this->next) {
            $searchArray = [];
            $replaceArray = [];
            /** @var Param $param */
            foreach($paramsCollection->getParams() as $param) {
                $search = $param->getName();
                if(is_null($replace = $param->getValue())) {
                    $replace = $param->getName();
                }

                if ($param->isNeedEncode()) {
                    $replace = urlencode($replace);
                }

                $searchArray[] = $search;
                $replaceArray[] = $replace;
            }

            return str_replace($searchArray, $replaceArray, $linkFormat);
        }

        return $this->next->generate($linkFormat, $paramsCollection, $options);
    }
}