<?php


namespace RevPush\ServicesBundle\LinkManager\Middleware;


interface MiddlewareInterface
{
    public function setNext(Middleware $middleware): Middleware;
}