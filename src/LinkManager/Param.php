<?php


namespace RevPush\ServicesBundle\LinkManager;


class Param
{
    public $id;
    public $name;
    public $description;
    public $urlParamName;
    public $value;
    public $gid;
    public $isStatic = false;
    public $needEncode = true;

    const KEYWORD = 'keyword';
    const KEYWORD_ID = 'keyword_id';
    const VISITOR_ID = 'visitor_id';
    const PARTNER_SHORT_NAME = 'partner_short_name';
    const PARTNER_ID = 'partner_id';
    const PARTNER_GID = 'partner_gid';
    const FEED_PARTNER_ID = 'feed_partner_id';
    const string FEED_PARTNER_GID = 'feed_partner_gid';
    const LINK_KEY = 'link_key';
    const USER_SHORT_NAME = 'buyer';
    const SOURCE_ID = 'source_id';
    const TARGET_ID = 'target_id';
    const CAMPAIGN_ID = 'campaign_id';
    const REDIRECT_PAGE = 'redirect_page';
    const SEARCH_CAMPAIGN_ID = 'search_campaign_id';
    const TRACKING_PARAMS = 'tracking_params';
    const SITE_DOMAIN = 'site_domain';
    const SITE_SHORT_NAME = 'site_short_name';
    const SITE_ID = 'site_id';
    const MARKET = 'market';
    const MARKET_GID = 'market_gid';
    const MARKET_ID = 'market_id';
    const LANGUAGE = 'language';
    const LANGUAGE_ID = 'language_id';
    const ADGROUP_SYSTEM_NAME = 'adgroup_system_name';
    const ADGROUP_ID = 'adgroup_id';
    const UTM_CONTENT = 'utm_content';
    const ACCOUNT_ID = 'account_id';
    const MANAGER_ACCOUNT_ID = 'manager_account_id';
    const BUSINESS_ID = 'business_id';
    const ACCOUNT_NAME = 'account_name';
    const USER_TEAM_ID = 'user_team_id';
    const USER_TEAM_NAME = 'user_team_name';
    const PARTNER_CHANNEL = 'partner_channel';
    const PARTNER_CHANNEL_ID = 'partner_channel_id';
    const PARTNER_CHANNEL_PARAM_CHANNEL_ID = 'partner_channel_channel_id';
    const PARTNER_CHANNEL_PARAM_STYLE_ID = 'partner_channel_style_id';
    const KEYWORD_REPLACEMENT = 'keyword_replacement';
    const START_DATE = 'start_date';
    const END_DATE = 'end_date';
    const SITE_PAGE = 'site_page';
    const CITY = 'city';
    const REFERER_AD_CREATIVE = 'referrer_ad_creative';
    const RELATED_TERMS = 'related_terms';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getUrlParamName()
    {
        return $this->urlParamName;
    }

    /**
     * @param mixed $urlParamName
     *
     * @return Param
     */
    public function setUrlParamName($urlParamName)
    {
        $this->urlParamName = $urlParamName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return Param
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGid()
    {
        return $this->gid;
    }

    /**
     * @return bool
     */
    public function isStatic(): bool
    {
        return $this->isStatic;
    }

    public function isNeedEncode(): bool
    {
        return $this->needEncode;
    }
}