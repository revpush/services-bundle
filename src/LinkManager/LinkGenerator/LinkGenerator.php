<?php


namespace RevPush\ServicesBundle\LinkManager\LinkGenerator;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Middleware\MiddlewareFactoryInterface;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;


class LinkGenerator
{
    private ?Middleware $middleware = null;
    private MiddlewareFactoryInterface $middlewareFactory;

    public function __construct(
        MiddlewareFactoryInterface $middlewareFactory
    )
    {
        $this->middlewareFactory = $middlewareFactory;
    }

    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string
    {
        return $this->getMiddleware()->generate($linkFormat, $paramsCollection, $options);
    }

    private function getMiddleware(): Middleware
    {
        if(!$this->middleware) {
            $this->middleware = $this->middlewareFactory->createMiddleware();
        }

        return $this->middleware;
    }

    public function withMiddlewares(array $list): self
    {
        $copy = clone $this;
        $copy->middleware = $copy->middlewareFactory->createMiddleware($list);

        return $copy;
    }
}