<?php


namespace RevPush\ServicesBundle\LinkManager\LinkGenerator;


use RevPush\ServicesBundle\LinkManager\ParamsCollection;

interface LinkGeneratorInterface
{
    public function generate(string $linkFormat, ParamsCollection $paramsCollection, array $options = []): string;
}