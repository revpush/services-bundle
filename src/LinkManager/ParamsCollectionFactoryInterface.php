<?php


namespace RevPush\ServicesBundle\LinkManager;


interface ParamsCollectionFactoryInterface
{
    public function createCollection(): ParamsCollection;
}