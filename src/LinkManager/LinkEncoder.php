<?php


namespace RevPush\ServicesBundle\LinkManager;


class LinkEncoder implements LinkEncoderInterface
{
    public function encode(string $link): string
    {
        return urlencode(base64_encode($link));
    }

    public function decode(string $hash): string
    {
        return base64_decode(urldecode($hash));
    }
}