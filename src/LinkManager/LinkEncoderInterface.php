<?php


namespace RevPush\ServicesBundle\LinkManager;


interface LinkEncoderInterface
{
    public function encode(string $link): string;
    public function decode(string $hash): string;
}