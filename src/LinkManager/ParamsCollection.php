<?php


namespace RevPush\ServicesBundle\LinkManager;


class ParamsCollection
{
    /**
     * @var array<string, Param>
     */
    private array $params = [];

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     *
     * @return ParamsCollection
     */
    public function setParams(array $params): ParamsCollection
    {
        $this->params = $params;

        return $this;
    }

    public function add(Param $param)
    {
        $this->params[$param->getGid()] = $param;
    }

    public function get($key): ?Param
    {
        if(isset($this->params[$key])) {
            return $this->params[$key];
        }

        return null;
    }

    public function isset($key): bool
    {
        return isset($this->params[$key]);
    }

    public function getValue($key)
    {
        if(isset($this->params[$key])) {
            return $this->params[$key]->getValue();
        }

        return null;
    }

    public function setValue($key, $value): bool
    {
        if(isset($this->params[$key])) {
            $this->params[$key]->setValue($value);
            return true;
        }

        return false;
    }
}
