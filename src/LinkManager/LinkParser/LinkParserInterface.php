<?php


namespace RevPush\ServicesBundle\LinkManager\LinkParser;


use RevPush\ServicesBundle\LinkManager\ParamsCollection;

interface LinkParserInterface
{
    public function parse(string $url, ParamsCollection $paramsCollection, array $options = []): void;
}