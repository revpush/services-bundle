<?php


namespace RevPush\ServicesBundle\LinkManager\LinkParser;


use RevPush\ServicesBundle\LinkManager\Middleware\Middleware;
use RevPush\ServicesBundle\LinkManager\Middleware\MiddlewareFactoryInterface;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\LinkManager\ParamsCollectionFactoryInterface;

class LinkParser
{
    private ?Middleware $middleware = null;
    private MiddlewareFactoryInterface $middlewareFactory;
    private ParamsCollectionFactoryInterface $paramsCollectionFactory;

    public function __construct(
        ParamsCollectionFactoryInterface $paramsCollectionFactory,
        MiddlewareFactoryInterface $middlewareFactory
    )
    {
        $this->middlewareFactory = $middlewareFactory;
        $this->paramsCollectionFactory = $paramsCollectionFactory;
    }

    public function parse(string $url, ?ParamsCollection $paramsCollection = null, array $options = []): ParamsCollection
    {
        if(is_null($paramsCollection)) {
            $paramsCollection = $this->paramsCollectionFactory->createCollection();
        }

        $this->getMiddleware()->parse($url, $paramsCollection, $options);

        return $paramsCollection;
    }

    private function getMiddleware(): Middleware
    {
        if(!$this->middleware) {
            $this->middleware = $this->middlewareFactory->createMiddleware();
        }

        return $this->middleware;
    }

    public function withMiddlewares(array $list): self
    {
        $copy = clone $this;
        $copy->middleware = $copy->middlewareFactory->createMiddleware($list);

        return $copy;
    }
}