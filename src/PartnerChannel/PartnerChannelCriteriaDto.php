<?php

namespace RevPush\ServicesBundle\PartnerChannel;

class PartnerChannelCriteriaDto
{
    public function __construct(
        private int $partnerId,
        private int $adgroupId,
        private int $keywordId,
    )
    {
    }

    public function getPartnerId(): int
    {
        return $this->partnerId;
    }

    public function getAdgroupId(): int
    {
        return $this->adgroupId;
    }

    public function getKeywordId(): int
    {
        return $this->keywordId;
    }
}