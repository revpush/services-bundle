<?php

namespace RevPush\ServicesBundle\PartnerChannel;

use RevPush\ServicesBundle\ApiRepository\PartnerApiRepository;
use RevPush\ServicesBundle\ApiRepository\PartnerChannelApiRepository;
use RevPush\ServicesBundle\ApiRepository\PartnerChannelParamApiRepository;
use Swagger\Client\ApiException;
use Swagger\Client\Model\KeywordKeywordRead;
use Swagger\Client\Model\PartnerChannelPartnerChannelRead;
use Swagger\Client\Model\PartnerPartnerRead;

class PartnerChannelRepository implements PartnerChannelRepositoryInterface
{
    public function __construct(
        private PartnerApiRepository $partnerApiRepository,
        private PartnerChannelApiRepository $partnerChannelApiRepository,
        private PartnerChannelParamApiRepository $partnerChannelApiParamRepository,
        private PartnerChannelSelector $partnerChannelSelector
    )
    {
    }

    /**
     * @throws ApiException
     */
    public function findOneById(int $partnerChannelId): ?PartnerChannelDto
    {
        return $this->createDtoFromApiEntity($this->partnerChannelApiRepository->find($partnerChannelId));
    }

    public function findOneByCriteria(PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto
    {
        $apiEntities = $this->partnerChannelApiRepository->findByAdgroupAndPartner(
            $criteria->getAdgroupId(), $criteria->getPartnerId()
        );

        $channels = array_map(
            fn(PartnerChannelPartnerChannelRead $apiEntity) => $this->createDtoFromApiEntity($apiEntity), $apiEntities
        );

        $strategy = $this->getPartnerChannelMappingStrategy($criteria->getPartnerId());

        return $this->partnerChannelSelector->select($channels, $strategy, $criteria);
    }

    public function findOneOrFillByCriteria(PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto
    {
        throw new \RuntimeException(__METHOD__ . ' is not supported');
    }

    public function isPartnerUsesChannels(int $partnerId): bool
    {
        $partner = $this->partnerApiRepository->find($partnerId);
        return $partner instanceof PartnerPartnerRead && $partner->getUseChannels();
    }

    private function getPartnerChannelMappingStrategy(int $partnerId): string
    {
        $partner = $this->partnerApiRepository->find($partnerId);
        return $partner instanceof PartnerPartnerRead ? $partner->getChannelMappingStrategy() : '';
    }

    /**
     * @throws ApiException
     */
    private function createDtoFromApiEntity(?PartnerChannelPartnerChannelRead $apiEntity): ?PartnerChannelDto
    {
        if (null === $apiEntity) {
            return null;
        }


        $keyword = null;
        /** @var KeywordKeywordRead|null $apiKeywordEntity */
        if ($apiKeywordEntity = $apiEntity->getKeyword()) {
            $keyword = new KeywordDto(
                $apiKeywordEntity->getId(),
                $apiKeywordEntity->getName(),
                $apiKeywordEntity->getIsPrimary()
            );
        }

        $dto = new PartnerChannelDto($apiEntity->getId(), $apiEntity->getChannel(), $keyword);

        if ($apiEntity->getChecksum()) {
            $apiParamEntities = $this->partnerChannelApiParamRepository->findByChannelId($apiEntity->getId());
            foreach ($apiParamEntities as $apiParamEntity) {
                $paramDto = new PartnerChannelParamDto($apiParamEntity->getName(), $apiParamEntity->getValue());
                $dto->addParam($paramDto);
            }
        }

        return $dto;
    }
}