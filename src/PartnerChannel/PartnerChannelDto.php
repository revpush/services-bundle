<?php

namespace RevPush\ServicesBundle\PartnerChannel;

class PartnerChannelDto
{
    private int $id;
    private string $channel;
    private array $params = [];
    private ?KeywordDto $keyword = null;

    public function __construct(int $id, string $channel, ?KeywordDto $keyword = null)
    {
        $this->id = $id;
        $this->channel = $channel;
        $this->keyword = $keyword;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getChannel(): string
    {
        return $this->channel;
    }

    public function getKeyword(): ?KeywordDto
    {
        return $this->keyword;
    }

    public function addParam(PartnerChannelParamDto $param): PartnerChannelDto
    {
        $this->params[] = $param;
        return $this;
    }

    /**
     * @return PartnerChannelParamDto[]
     */
    public function getParams(): array
    {
        return $this->params;
    }
}