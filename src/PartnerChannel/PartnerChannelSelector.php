<?php

namespace RevPush\ServicesBundle\PartnerChannel;

class PartnerChannelSelector
{
    /**
     * @param PartnerChannelDto[] $channels
     * @param string $strategy
     * @return PartnerChannelDto|null
     */
    public function select(array $channels, string $strategy, PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto
    {
        switch ($strategy) {
            case PartnerChannelRepositoryInterface::MAPPING_STRATEGY_ADGROUP:
                return $this->choosePartnerChannelByAdgroupStrategy($channels, $criteria);

            case PartnerChannelRepositoryInterface::MAPPING_STRATEGY_KEYWORD:
                return $this->choosePartnerChannelByKeywordStrategy($channels, $criteria);

            default:
                throw new \InvalidArgumentException('Unknown mapping strategy provided');
        }
    }

    /**
     * @param PartnerChannelDto[] $channels
     * @param PartnerChannelCriteriaDto $criteria
     * @return PartnerChannelDto|null
     */
    private function choosePartnerChannelByAdgroupStrategy(array $channels, PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto
    {
        return array_shift($channels);
    }

    /**
     * @param PartnerChannelDto[] $channels
     * @param PartnerChannelCriteriaDto $criteria
     * @return PartnerChannelDto|null
     */
    private function choosePartnerChannelByKeywordStrategy(array $channels, PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto
    {
        foreach ($channels as $channel) {
            if (!$keyword = $channel->getKeyword()) {
                continue;
            }

            if ($keyword->getId() === $criteria->getKeywordId()) {
                return $channel;
            }
        }

        foreach ($channels as $channel) {
            if (!$keyword = $channel->getKeyword()) {
                continue;
            }

            if ($keyword->isPrimary()) {
                return $channel;
            }
        }

        return array_shift($channels);
    }
}