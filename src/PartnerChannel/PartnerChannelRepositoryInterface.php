<?php

namespace RevPush\ServicesBundle\PartnerChannel;

interface PartnerChannelRepositoryInterface
{
    public const MAPPING_STRATEGY_ADGROUP = 'adgroup';
    public const MAPPING_STRATEGY_KEYWORD = 'keyword';

    public function findOneById(int $partnerChannelId): ?PartnerChannelDto;

    public function findOneByCriteria(PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto;

    public function findOneOrFillByCriteria(PartnerChannelCriteriaDto $criteria): ?PartnerChannelDto;

    public function isPartnerUsesChannels(int $partnerId): bool;
}