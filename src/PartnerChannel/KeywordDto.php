<?php

namespace RevPush\ServicesBundle\PartnerChannel;

class KeywordDto
{
    public function __construct(
        private int $id,
        private string $name,
        private bool $isPrimary,
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }
}