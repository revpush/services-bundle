<?php

namespace RevPush\ServicesBundle\ParamDefiner;

use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\ApiRepository\FeedZoneApiRepository;
use RevPush\ServicesBundle\ApiRepository\PartnerApiRepository;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use Swagger\Client\Model\FeedZoneFeedZoneRead;
use Swagger\Client\Model\PartnerPartnerRead;

readonly class FeedPartnerDefiner
{
    public function __construct(
        private FeedZoneApiRepository $feedZoneApiRepository,
        private PartnerApiRepository $partnerApiRepository,
        private AdgroupApiRepository $adgroupApiRepository
    ) {}

    public function getFeedPartnerId(ParamsCollection $linkFormatParams, ?int $feedZoneId = null): ?int
    {
        if (!is_null($feedZoneId)) {
            /** @var FeedZoneFeedZoneRead $feedZone */
            if ($feedZone = $this->feedZoneApiRepository->find($feedZoneId)) {
                return $feedZone->getPartner()->getId();
            }
        }

        if ($feedPartnerId = $linkFormatParams->getValue(Param::FEED_PARTNER_ID)) {
            return $feedPartnerId;
        }

        if ($adgroup = $this->adgroupApiRepository->findByLinkKey($linkFormatParams->getValue(Param::LINK_KEY))) {
            return $adgroup->getSearchPartner()->getId();
        }

        return null;
    }

    public function getFeedPartnerGid(ParamsCollection $linkFormatParams, ?int $feedZoneId = null): ?string
    {
        if ($feedPartnerGid = $linkFormatParams->getValue(Param::FEED_PARTNER_GID)) {
            return $feedPartnerGid;
        }

        if ($feedPartnerId = $this->getFeedPartnerId($linkFormatParams, $feedZoneId)) {
            /** @var PartnerPartnerRead|null $feedPartner */
            $feedPartner = $this->partnerApiRepository->find($feedPartnerId);
            return $feedPartner?->getGid();
        }

        return null;
    }
}
