<?php

namespace RevPush\ServicesBundle\ParamDefiner;

use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\ApiRepository\FeedZoneApiRepository;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use Swagger\Client\Model\FeedZoneFeedZoneRead;

readonly class FeedPartnerDefiner
{
    public function __construct(
        private FeedZoneApiRepository $feedZoneApiRepository,
        private AdgroupApiRepository $adgroupApiRepository
    ) {}

    public function getFeedPartnerId(ParamsCollection $linkFormatParams, ?int $feedZoneId = null): ?int
    {
        if (!is_null($feedZoneId)) {
            /** @var FeedZoneFeedZoneRead $feedZone */
            if ($feedZone = $this->feedZoneApiRepository->find($feedZoneId)) {
                return $feedZone->getPartner()->getId();
            }
        }

        if ($feedPartnerId = $linkFormatParams->getValue(Param::FEED_PARTNER_ID)) {
            return $feedPartnerId;
        }

        if ($adgroup = $this->adgroupApiRepository->findByLinkKey($linkFormatParams->getValue(Param::LINK_KEY))) {
            return $adgroup->getSearchPartner()->getId();
        }

        return null;
    }
}
