<?php

namespace RevPush\ServicesBundle\ConversionSender\Organic\RuslanTeam;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;
use RevPush\ServicesBundle\ConversionSender\ConversionResponse;
use RevPush\ServicesBundle\ConversionSender\ConversionSenderInterface;

class RuslanTeamConversionSender implements ConversionSenderInterface
{
    private RuslanTeamApiClient $apiClient;

    public function __construct(
        RuslanTeamApiClient $apiClient
    )
    {
        $this->apiClient = $apiClient;
    }

    public static function getConversionClass(): string
    {
        return RuslanTeamConversion::class;
    }

    public function send(AbstractConversion $conversion): ConversionResponse
    {
        if (!$conversion instanceof RuslanTeamConversion) {
            throw new \InvalidArgumentException('Wrong type of conversion');
        }

        $request = $this->apiClient->createRequest($conversion);
        return $this->apiClient->getConversionResponse($request);
    }
}
