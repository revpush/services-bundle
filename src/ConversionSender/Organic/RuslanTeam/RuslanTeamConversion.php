<?php

namespace RevPush\ServicesBundle\ConversionSender\Organic\RuslanTeam;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;

class RuslanTeamConversion extends AbstractConversion
{
    private string $trId;

    public function __construct(string $visitorId, string $trId)
    {
        parent::__construct($visitorId);

        $this->trId = $trId;
    }

    public function getTrId(): string
    {
        return $this->trId;
    }
}
