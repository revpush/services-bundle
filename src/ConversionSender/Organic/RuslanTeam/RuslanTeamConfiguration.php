<?php

namespace RevPush\ServicesBundle\ConversionSender\Organic\RuslanTeam;

use RevPush\ServicesBundle\ConversionSender\ClientConfiguration;

class RuslanTeamConfiguration extends ClientConfiguration
{
    private string $conversionUrl;

    public function __construct(string $conversionUrl)
    {
        $this->conversionUrl = $conversionUrl;
    }

    public function getConversionUrl(): string
    {
        return $this->conversionUrl;
    }
}
