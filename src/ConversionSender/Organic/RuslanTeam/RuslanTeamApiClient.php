<?php

namespace RevPush\ServicesBundle\ConversionSender\Organic\RuslanTeam;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;
use RevPush\ServicesBundle\ConversionSender\ConversionResponse;
use RevPush\ServicesBundle\ConversionSender\HttpClientConversionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RuslanTeamApiClient implements HttpClientConversionInterface
{
    private HttpClientInterface $httpClient;
    private RuslanTeamConfiguration $configuration;

    private const TR_ID_PARAM_NAME = '{tr_id}';

    public function __construct(
        HttpClientInterface $httpClient,
        RuslanTeamConfiguration $configuration
    )
    {
        $this->httpClient = $httpClient;
        $this->configuration = $configuration;
    }

    /**
     * @param RuslanTeamConversion $conversion
     * @return ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function createRequest(AbstractConversion $conversion): ResponseInterface
    {
        $formattedConversionLink = $this->prepareConversionLink($conversion);
        return $this->httpClient->request('GET', $formattedConversionLink, ['timeout' => $this->configuration->getTimeout()]);
    }

    public function getConversionResponse(ResponseInterface $request): ConversionResponse
    {
        if ($request->getStatusCode() === Response::HTTP_OK) {
            return ConversionResponse::createSuccessResponse();
        }

        return ConversionResponse::createErrorResponse($request->getStatusCode(), $request->getContent());
    }

    private function prepareConversionLink(RuslanTeamConversion $conversion)
    {
        return str_replace(self::TR_ID_PARAM_NAME, $conversion->getTrId(), $this->configuration->getConversionUrl());
    }
}
