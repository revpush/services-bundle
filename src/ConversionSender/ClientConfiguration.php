<?php

namespace RevPush\ServicesBundle\ConversionSender;

abstract class ClientConfiguration
{
    protected int $timeout = 1;
    protected bool $waitAnswer = false;

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout): ClientConfiguration
    {
        $this->timeout = $timeout;
        return $this;
    }

    public function isWaitAnswer(): bool
    {
        return $this->waitAnswer;
    }

    public function setWaitAnswer(bool $waitAnswer, int $timeout = 5): ClientConfiguration
    {
        if($waitAnswer) {
            $this->timeout = $timeout;
        } else {
            $this->timeout = 1;
        }

        $this->waitAnswer = $waitAnswer;
        return $this;
    }
}