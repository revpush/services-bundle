<?php

namespace RevPush\ServicesBundle\ConversionSender;

class ConversionResponse
{
    private string $status = self::STATUS_SUCCESS;
    private int $statusCode = 200;
    private ?string $message = null;

    public const STATUS_SUCCESS = 'Success';
    public const STATUS_ERROR = 'Error';

    public static function createSuccessResponse(?string $message = null): self
    {
        return (new ConversionResponse())
            ->setMessage($message);
    }

    public static function createErrorResponse(int $statusCode = null, ?string $message = null): self
    {
        return (new ConversionResponse())
            ->setStatus(self::STATUS_ERROR)
            ->setStatusCode($statusCode)
            ->setMessage($message);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return ConversionResponse
     */
    protected function setStatus(string $status): ConversionResponse
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return ConversionResponse
     */
    protected function setStatusCode(int $statusCode): ConversionResponse
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return ConversionResponse
     */
    protected function setMessage(?string $message): ConversionResponse
    {
        $this->message = $message;
        return $this;
    }
}