<?php

namespace RevPush\ServicesBundle\ConversionSender;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface HttpClientConversionInterface
{
    public function createRequest(AbstractConversion $conversion): ResponseInterface;
    public function getConversionResponse(ResponseInterface $request): ConversionResponse;
}