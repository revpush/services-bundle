<?php

namespace RevPush\ServicesBundle\ConversionSender;

class ConversionSenderCollection
{
    private array $collection = [];

    public function getAll(): array
    {
        return $this->collection;
    }

    public function get(string $conversionClass): ?ConversionSenderInterface
    {
        return $this->collection[$conversionClass] ?? null;
    }

    public function set(ConversionSenderInterface $conversionSender)
    {
        $this->collection[$conversionSender::getConversionClass()] = $conversionSender;
    }
}