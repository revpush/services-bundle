<?php

namespace RevPush\ServicesBundle\ConversionSender\Base;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;
use RevPush\ServicesBundle\ConversionSender\ConversionResponse;
use RevPush\ServicesBundle\ConversionSender\ConversionSenderInterface;

class BaseConversionSender implements ConversionSenderInterface
{
    private BaseApiClient $baseApiClient;

    public function __construct(BaseApiClient $baseApiClient)
    {
        $this->baseApiClient = $baseApiClient;
    }

    public static function create(?BaseApiClient $baseApiClient = null, bool $waiteAnswer = true): BaseConversionSender
    {
        $baseApiClient = !is_null($baseApiClient) ? $baseApiClient : BaseApiClient::create(null, null, $waiteAnswer);

        return new BaseConversionSender($baseApiClient);
    }

    public static function getConversionClass(): string
    {
        return BaseConversion::class;
    }

    public function send(AbstractConversion $conversion): ConversionResponse
    {
        if(!$conversion instanceof BaseConversion) {
            throw new \InvalidArgumentException('Wrong type of conversion');
        }

        $request = $this->baseApiClient->createRequest($conversion);
        return $this->baseApiClient->getConversionResponse($request);
    }
}