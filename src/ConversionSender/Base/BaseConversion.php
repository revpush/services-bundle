<?php

namespace RevPush\ServicesBundle\ConversionSender\Base;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;

class BaseConversion extends AbstractConversion
{
    private string $conversionUrl;

    public function __construct(string $visitorId, string $conversionUrl, float $rpc = 0.00)
    {
        parent::__construct($visitorId, $rpc);

        $this->conversionUrl = $conversionUrl;
    }

    public function getConversionUrl(): string
    {
        return $this->conversionUrl;
    }
}