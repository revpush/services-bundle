<?php

namespace RevPush\ServicesBundle\ConversionSender\Base;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;
use RevPush\ServicesBundle\ConversionSender\ConversionResponse;
use RevPush\ServicesBundle\ConversionSender\HttpClientConversionInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class BaseApiClient implements HttpClientConversionInterface
{
    private HttpClientInterface $httpClient;
    private BaseConfiguration $configuration;

    private const VISITOR_ID_PARAM_NAME = '{visitor_id}';
    private const RPC_PARAM_NAME = '{rpc}';

    public function __construct(
        HttpClientInterface $httpClient,
        BaseConfiguration $configuration
    )
    {
        $this->httpClient = $httpClient;
        $this->configuration = $configuration;
    }

    public static function create(?HttpClientInterface $httpClient = null, ?BaseConfiguration $configuration = null, bool $waitAnswer = true): BaseApiClient
    {
        $httpClient = !is_null($httpClient) ? $httpClient : HttpClient::create();
        $configuration = !is_null($configuration) ? $configuration : (new BaseConfiguration())->setWaitAnswer($waitAnswer);

        return new BaseApiClient($httpClient, $configuration);
    }

    /**
     * @param BaseConversion $conversion
     * @return ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function createRequest(AbstractConversion $conversion): ResponseInterface
    {
        $formattedConversionLink = $this->prepareConversionLink($conversion);
        return $this->httpClient->request('GET', $formattedConversionLink, ['timeout' => $this->configuration->getTimeout()]);
    }

    public function getConversionResponse(ResponseInterface $request): ConversionResponse
    {
        if ($request->getStatusCode() === Response::HTTP_OK) {
            return ConversionResponse::createSuccessResponse();
        }

        return ConversionResponse::createErrorResponse($request->getStatusCode(), $request->getContent());
    }

    private function prepareConversionLink(BaseConversion $conversion): string
    {
        return str_replace(
            [self::VISITOR_ID_PARAM_NAME, self::RPC_PARAM_NAME],
            [$conversion->getVisitorId(), $conversion->getRpc()],
            $conversion->getConversionUrl()
        );
    }
}