<?php

namespace RevPush\ServicesBundle\ConversionSender;

interface ConversionSenderInterface
{
    public static function getConversionClass(): string;

    /**
     * @param AbstractConversion $conversion
     * @return ConversionResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function send(AbstractConversion $conversion): ConversionResponse;
}