<?php

namespace RevPush\ServicesBundle\ConversionSender\TikTok;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;
use RevPush\ServicesBundle\ConversionSender\ConversionResponse;
use RevPush\ServicesBundle\ConversionSender\HttpClientConversionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class TikTokApiClient implements HttpClientConversionInterface
{
    private TikTokConfiguration $configuration;
    private HttpClientInterface $httpClient;

    private const SUCCESS_RESPONSE_CONTENT_CODE = 0;

    public function __construct(
        HttpClientInterface $httpClient,
        TikTokConfiguration $configuration
    )
    {
        $this->httpClient = $httpClient;
        $this->configuration = $configuration;
    }

    /**
     * @param TikTokConversion $conversion
     * @return ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function createRequest(AbstractConversion $conversion): ResponseInterface
    {
        $options = [
            'headers' => [
                'Access-Token' => $conversion->getAccessToken(),
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'pixel_code' => $conversion->getPixelId(),
                'event' => 'ViewContent',
                'timestamp' => $conversion->getConversionDate()->format('Y-m-d\TH:i:s\Z'),
                'context' => [
                    'ad' => [
                        'callback' => $conversion->getVisitorId()
                    ],
                    'page' => [
                        'url' => $conversion->getRequestUri()
                    ],
                    'user_agent' => $conversion->getUserAgent(),
                    'ip' => $conversion->getUserIp()
                ]
            ],
            'timeout' => $this->configuration->getTimeout()
        ];

        return $this->httpClient->request('POST', $this->configuration->getConversionUrl(), $options);
    }

    public function getConversionResponse(ResponseInterface $request): ConversionResponse
    {
        if ($request->getStatusCode() === Response::HTTP_OK && $request->toArray(false)['code'] === self::SUCCESS_RESPONSE_CONTENT_CODE) {
            return ConversionResponse::createSuccessResponse();
        }

        return ConversionResponse::createErrorResponse($request->getStatusCode(), $request->toArray(false)['message']);
    }
}