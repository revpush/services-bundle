<?php

namespace RevPush\ServicesBundle\ConversionSender\TikTok;

use RevPush\ServicesBundle\ConversionSender\ClientConfiguration;

class TikTokConfiguration extends ClientConfiguration
{
    private string $conversionUrl;

    public function __construct(string $conversionUrl)
    {
        $this->conversionUrl = $conversionUrl;
    }

    public function getConversionUrl(): string
    {
        return $this->conversionUrl;
    }
}