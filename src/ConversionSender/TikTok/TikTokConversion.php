<?php

namespace RevPush\ServicesBundle\ConversionSender\TikTok;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;

class TikTokConversion extends AbstractConversion
{
    public const EVENT_VIEW = 'ViewContent';

    private string $pixelId;
    private \DateTime $conversionDate;
    private string $event = self::EVENT_VIEW;
    private ?string $requestUri = null;
    private string $userIp;
    private string $userAgent;
    private string $accessToken;

    public function __construct(
        string $pixelId,
        string $visitorId,
        string $userIp,
        string $userAgent,
        string $accessToken
    )
    {
        parent::__construct($visitorId);

        $this->pixelId = $pixelId;
        $this->conversionDate = new \DateTime();
        $this->userIp = $userIp;
        $this->userAgent = $userAgent;
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getPixelId(): string
    {
        return $this->pixelId;
    }

    /**
     * @return \DateTime
     */
    public function getConversionDate(): \DateTime
    {
        return $this->conversionDate;
    }

    /**
     * @param \DateTime $conversionDate
     * @return TikTokConversion
     */
    public function setConversionDate(\DateTime $conversionDate): TikTokConversion
    {
        $this->conversionDate = $conversionDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     * @return TikTokConversion
     */
    public function setEvent(string $event): TikTokConversion
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequestUri(): ?string
    {
        return $this->requestUri;
    }

    /**
     * @param string|null $requestUri
     * @return TikTokConversion
     */
    public function setRequestUri(?string $requestUri): TikTokConversion
    {
        $this->requestUri = $requestUri;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserIp(): string
    {
        return $this->userIp;
    }

    /**
     * @param string $userIp
     * @return TikTokConversion
     */
    public function setUserIp(string $userIp): TikTokConversion
    {
        $this->userIp = $userIp;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     * @return TikTokConversion
     */
    public function setUserAgent(string $userAgent): TikTokConversion
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }
}