<?php

namespace RevPush\ServicesBundle\ConversionSender\TikTok;

use RevPush\ServicesBundle\ConversionSender\AbstractConversion;
use RevPush\ServicesBundle\ConversionSender\ConversionResponse;
use RevPush\ServicesBundle\ConversionSender\ConversionSenderInterface;

class TikTokConversionSender implements ConversionSenderInterface
{
    private TikTokApiClient $tikTokApiClient;

    public function __construct(
        TikTokApiClient $tikTokApiClient
    )
    {
        $this->tikTokApiClient = $tikTokApiClient;
    }

    public static function getConversionClass(): string
    {
        return TikTokConversion::class;
    }

    public function send(AbstractConversion $conversion): ConversionResponse
    {
        if(!$conversion instanceof TikTokConversion) {
            throw new \InvalidArgumentException('Wrong type of conversion');
        }

        $request = $this->tikTokApiClient->createRequest($conversion);
        return $this->tikTokApiClient->getConversionResponse($request);
    }
}