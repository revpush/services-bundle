<?php

namespace RevPush\ServicesBundle\ConversionSender;

abstract class AbstractConversion
{
    protected string $visitorId;
    protected float $rpc;

    public function __construct(string $visitorId, float $rpc = 0.00)
    {
        $this->visitorId = $visitorId;
        $this->rpc = $rpc;
    }

    public function getVisitorId(): string
    {
        return $this->visitorId;
    }

    public function getRpc(): float
    {
        return $this->rpc;
    }
}