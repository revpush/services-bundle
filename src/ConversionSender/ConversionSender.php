<?php

namespace RevPush\ServicesBundle\ConversionSender;

use Psr\Log\LoggerInterface;

class ConversionSender
{
    private ConversionSenderCollection $collection;
    private LoggerInterface $logger;

    public function __construct(
        ConversionSenderCollection $collection,
        LoggerInterface $logger
    )
    {
        $this->collection = $collection;
        $this->logger = $logger;
    }

    public function send(AbstractConversion $conversion): ?ConversionResponse
    {
        try {
            if ($conversionSender = $this->collection->get(get_class($conversion))) {
                return $conversionSender->send($conversion);
            }

            return null;
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
            return ConversionResponse::createErrorResponse($exception->getCode(), $exception->getMessage());
        }
    }
}
