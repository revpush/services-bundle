<?php

namespace RevPush\ServicesBundle\UserParams;

class RequestParams
{
    private ?string $requestUrl = null;
    private ?string $requestPath = null;
    private ?string $userIp = null;
    private ?string $userAgent = null;
    private ?string $userReferer = null;
    private ?string $sessionId = null;
    private ?int $deviceId = null;

    /**
     * @return string|null
     */
    public function getRequestUrl(): ?string
    {
        return $this->requestUrl;
    }

    /**
     * @param string|null $requestUrl
     * @return RequestParams
     */
    public function setRequestUrl(?string $requestUrl): RequestParams
    {
        $this->requestUrl = $requestUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequestPath(): ?string
    {
        return $this->requestPath;
    }

    /**
     * @param string|null $requestPath
     * @return RequestParams
     */
    public function setRequestPath(?string $requestPath): RequestParams
    {
        $this->requestPath = $requestPath;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserIp(): ?string
    {
        return $this->userIp;
    }

    /**
     * @param string|null $userIp
     * @return RequestParams
     */
    public function setUserIp(?string $userIp): RequestParams
    {
        $this->userIp = $userIp;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @param string|null $userAgent
     * @return RequestParams
     */
    public function setUserAgent(?string $userAgent): RequestParams
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserReferer(): ?string
    {
        return $this->userReferer;
    }

    /**
     * @param string|null $userReferer
     * @return RequestParams
     */
    public function setUserReferer(?string $userReferer): RequestParams
    {
        $this->userReferer = $userReferer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    /**
     * @param string|null $sessionId
     * @return RequestParams
     */
    public function setSessionId(?string $sessionId): RequestParams
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeviceId(): ?int
    {
        return $this->deviceId;
    }

    /**
     * @param int|null $deviceId
     * @return RequestParams
     */
    public function setDeviceId(?int $deviceId): RequestParams
    {
        $this->deviceId = $deviceId;
        return $this;
    }
}