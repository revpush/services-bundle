<?php

namespace RevPush\ServicesBundle\UserParams;

class GeoParams
{
    private ?string $userIp = null;
    private ?string $zipCode = null;
    private ?string $city = null;
    private ?string $countryCode = null;
    private ?string $languageCode = null;
    private ?string $state = null;
    private ?float $latitude = null;
    private ?float $longitude = null;
    private ?int $marketId = null;
    private ?string $marketMainLanguageGid = null;

    /**
     * @return string|null
     */
    public function getUserIp(): ?string
    {
        return $this->userIp;
    }

    /**
     * @param string|null $userIp
     * @return GeoParams
     */
    public function setUserIp(?string $userIp): GeoParams
    {
        $this->userIp = $userIp;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param string|null $zipCode
     * @return GeoParams
     */
    public function setZipCode(?string $zipCode): GeoParams
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return GeoParams
     */
    public function setCity(?string $city): GeoParams
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @param string|null $countryCode
     * @return GeoParams
     */
    public function setCountryCode(?string $countryCode): GeoParams
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    /**
     * @param string|null $languageCode
     * @return GeoParams
     */
    public function setLanguageCode(?string $languageCode): GeoParams
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return GeoParams
     */
    public function setState(?string $state): GeoParams
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float|null $latitude
     * @return GeoParams
     */
    public function setLatitude(?float $latitude): GeoParams
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float|null $longitude
     * @return GeoParams
     */
    public function setLongitude(?float $longitude): GeoParams
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMarketId(): ?int
    {
        return $this->marketId;
    }

    /**
     * @param int|null $marketId
     * @return GeoParams
     */
    public function setMarketId(?int $marketId): GeoParams
    {
        $this->marketId = $marketId;
        return $this;
    }

    public function getMarketMainLanguageGid(): ?string
    {
        return $this->marketMainLanguageGid;
    }

    public function setMarketMainLanguageGid(?string $marketMainLanguageGid): GeoParams
    {
        $this->marketMainLanguageGid = $marketMainLanguageGid;
        return $this;
    }
}