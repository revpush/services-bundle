<?php

namespace RevPush\ServicesBundle\UserParams;

use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\PageData\PageDataParamsInterface;

class UserParamsCollection
{
    private RequestParams $requestParams;
    private GeoParams $geoParams;
    private ParamsCollection $linkFormatParams;
    private array $pageDataParamsCollection = [];

    public function __construct(
        RequestParams $requestParams,
        GeoParams $geoParams,
        ParamsCollection $linkFormatParams
    )
    {
        $this->requestParams = $requestParams;
        $this->geoParams = $geoParams;
        $this->linkFormatParams = $linkFormatParams;
    }

    /** @var array<string, mixed> */
    private array $params = [];

    public function getParams(): array
    {
        return $this->params;
    }

    public function addParam(string $name, $value)
    {
        $this->params[$name] = $value;
    }

    public function getParam(string $name, $default = null)
    {
        return $this->params[$name] ?? $default;
    }

    /**
     * @return RequestParams
     */
    public function getRequestParams(): RequestParams
    {
        return $this->requestParams;
    }

    /**
     * @param RequestParams $requestParams
     * @return UserParamsCollection
     */
    public function setRequestParams(RequestParams $requestParams): UserParamsCollection
    {
        $copy = clone $this;
        $copy->requestParams = $requestParams;

        return $copy;
    }

    /**
     * @return GeoParams
     */
    public function getGeoParams(): GeoParams
    {
        return $this->geoParams;
    }

    /**
     * @param GeoParams $geoParams
     * @return UserParamsCollection
     */
    public function setGeoParams(GeoParams $geoParams): UserParamsCollection
    {
        $copy = clone $this;
        $copy->geoParams = $geoParams;

        return $copy;
    }

    /**
     * @return ParamsCollection
     */
    public function getLinkFormatParams(): ParamsCollection
    {
        return $this->linkFormatParams;
    }

    /**
     * @param ParamsCollection $linkFormatParams
     * @return UserParamsCollection
     */
    public function setLinkFormatParams(ParamsCollection $linkFormatParams): UserParamsCollection
    {
        $copy = clone $this;
        $copy->linkFormatParams = $linkFormatParams;

        return $copy;
    }

    public function addOrUpdatePageDataToCollection(PageDataParamsInterface $pageDataParams): void
    {
        $this->pageDataParamsCollection[get_class($pageDataParams)] = $pageDataParams;
    }

    public function getPageDataFromCollection(string $class): ?PageDataParamsInterface
    {
        return $this->pageDataParamsCollection[$class] ?? null;
    }

    /**
     * @return array
     */
    public function getPageDataParamsCollection(): array
    {
        return $this->pageDataParamsCollection;
    }

    /**
     * @param array $pageDataParamsCollection
     * @return UserParamsCollection
     */
    public function setPageDataParamsCollection(array $pageDataParamsCollection): UserParamsCollection
    {
        $this->pageDataParamsCollection = $pageDataParamsCollection;
        return $this;
    }
}