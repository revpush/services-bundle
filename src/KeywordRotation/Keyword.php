<?php


namespace RevPush\ServicesBundle\KeywordRotation;


use Swagger\Client\Model\KeywordKeywordRead;

class Keyword
{
    private $id;
    private $name;
    private $weight;
    private $status;
    private bool $isPrimary = false;
    private bool $useReplacement = false;
    private bool $isRotation = false;

    /**
     * @param KeywordKeywordRead|mixed $keywordObject
     * @return self
     */
    public static function createFromApiEntity(object $keywordObject): self
    {
        $self = new self();
        $self->id = $keywordObject->getId();
        $self->name = $keywordObject->getName();
        $self->weight = $keywordObject->getWeight();
        $self->status = $keywordObject->getStatus();
        $self->isPrimary = $keywordObject->getIsPrimary();
        $self->useReplacement = $keywordObject->getUseReplacement();
        $self->isRotation = $keywordObject->getIsRotation();
        return $self;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }

    /**
     * @return bool
     */
    public function isUseReplacement(): bool
    {
        return $this->useReplacement;
    }

    /**
     * @return bool
     */
    public function isRotation(): bool
    {
        return $this->isRotation;
    }
}