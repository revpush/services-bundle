<?php


namespace RevPush\ServicesBundle\KeywordRotation;



class KeywordRotationService
{
    /**
     * @param Keyword[] $keywords
     * @return Keyword
     */
    public function getKeywordRotation(array $keywords): Keyword
    {
        $keywords = $this->getAvailableKeywordsForRotation($keywords);

        if (count($keywords) === 1) {
            return array_shift($keywords);
        }

        $resultKeyword = null;
        $total = 0;
        foreach($keywords as $keyword) {
            $total += $keyword->getWeight();
        }
        $rand = mt_rand(0, $total - 1);
        $prev_number = 0;
        foreach ($keywords as $keyword) {
            if($rand >= $prev_number && $rand < $prev_number + $keyword->getWeight()) {
                $resultKeyword = $keyword;
                break;
            }
            $prev_number += $keyword->getWeight();
        }

        return $resultKeyword;
    }

    /**
     * @param Keyword[] $keywords
     * @return array
     */
    public function getAvailableKeywordsForRotation(array $keywords): array
    {
        return array_filter(
            $keywords, fn(Keyword $keyword) => $keyword->getStatus() && $keyword->isRotation()
        );
    }
}