<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\LinkFormatTypeApi;
use Swagger\Client\Model\LinkFormatTypeLinkFormatTypeRead;

class LinkFormatTypeApiRepository extends AbstractApiRepository
{
    private LinkFormatTypeApi $api;

    public function __construct(LinkFormatTypeApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getLinkFormatTypeItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getLinkFormatTypeCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getLinkFormatTypeCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return LinkFormatTypeLinkFormatTypeRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/link_format_types';
    }
}