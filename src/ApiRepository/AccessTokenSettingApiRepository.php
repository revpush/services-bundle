<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\AccessTokenSettingApi;
use Swagger\Client\Model\AccessTokenSettingAccessTokenSettingRead;
use Swagger\Client\Model\ModelInterface;

class AccessTokenSettingApiRepository extends AbstractApiRepository
{
    private AccessTokenSettingApi $api;

    public const ACCESS_TOKEN_ID_FILTER = 'access_token';
    public const PARTNER_ID_FILTER = 'partner';
    public const SITE_ID_FILTER = 'site';

    public function __construct(AccessTokenSettingApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?ModelInterface
    {
        return $this->api->getAccessTokenSettingItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getAccessTokenSettingCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getAccessTokenSettingCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return AccessTokenSettingAccessTokenSettingRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/access_token_settings';
    }
}