<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\LinkFormatParamApi;
use Swagger\Client\Model\LinkFormatParamLinkFormatParamRead;

class LinkFormatParamApiRepository extends AbstractApiRepository
{
    private LinkFormatParamApi $api;

    public function __construct(LinkFormatParamApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getLinkFormatParamItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getLinkFormatParamCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getLinkFormatParamCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return LinkFormatParamLinkFormatParamRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/link_format_params';
    }

}