<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteGroupApi;
use Swagger\Client\Model\SiteGroupSiteGroupRead;

class SiteGroupApiRepository extends AbstractApiRepository
{
    private SiteGroupApi $api;

    public function __construct(SiteGroupApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getSiteGroupItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSiteGroupCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteGroupCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteGroupSiteGroupRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_groups';
    }
}