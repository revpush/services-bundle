<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\PartnerMarketApi;
use Swagger\Client\Model\PartnerMarketPartnerMarketRead;

class PartnerMarketApiRepository extends AbstractApiRepository
{
    private PartnerMarketApi $api;

    public const MARKET_FILTER = 'market';
    public const PARTNER_FILTER = 'partner';

    public function __construct(PartnerMarketApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getPartnerMarketItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getPartnerMarketCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getPartnerMarketCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return PartnerMarketPartnerMarketRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/partner_markets';
    }
}