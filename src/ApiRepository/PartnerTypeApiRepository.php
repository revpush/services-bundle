<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\PartnerTypeApi;
use Swagger\Client\Model\PartnerTypePartnerTypeRead;

class PartnerTypeApiRepository extends AbstractApiRepository
{
    private PartnerTypeApi $api;

    public function __construct(PartnerTypeApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getPartnerTypeItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getPartnerTypeCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getPartnerTypeCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return PartnerTypePartnerTypeRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/partner_types';
    }
}