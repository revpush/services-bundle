<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\FeedZoneApi;
use Swagger\Client\Model\FeedZoneFeedZoneRead;

class FeedZoneApiRepository extends AbstractApiRepository
{
    private FeedZoneApi $api;

    public const SITE_ID_FILTER = 'site';

    public function __construct(FeedZoneApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getFeedZoneItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getFeedZoneCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getFeedZoneCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return FeedZoneFeedZoneRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/feed_zones';
    }

    public function getIdentifierFieldsForCollection(): array
    {
        return [
            'feed_zone_settings' => ['id' => 'feed_zone']
        ];
    }
}