<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\LinkFormatParamSiteApi;
use Swagger\Client\Model\LinkFormatParamSiteLinkFormatParamSiteRead;

class LinkFormatParamSiteApiRepository extends AbstractApiRepository
{
    private LinkFormatParamSiteApi $api;

    public const SITE_ID_FILTER = 'site';

    public function __construct(LinkFormatParamSiteApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getLinkFormatParamSiteItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getLinkFormatParamSiteCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getLinkFormatParamSiteCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return LinkFormatParamSiteLinkFormatParamSiteRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/link_format_param_sites';
    }
}