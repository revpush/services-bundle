<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\FeedZoneSettingApi;
use Swagger\Client\Model\FeedZoneSettingFeedZoneSettingRead;

class FeedZoneSettingApiRepository extends AbstractApiRepository
{
    private FeedZoneSettingApi $api;

    public const FEED_ZONE_ID_FILTER = 'feed_zone';
    public const PARTNER_GID_FILTER = 'partner_gid';

    public function __construct(FeedZoneSettingApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getFeedZoneSettingItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getFeedZoneSettingCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getFeedZoneSettingCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return FeedZoneSettingFeedZoneSettingRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/feed_zone_settings';
    }

}