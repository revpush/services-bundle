<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\StatClickTypeApi;
use Swagger\Client\Model\StatClickType;

class StatClickTypeApiRepository extends AbstractApiRepository
{
    private StatClickTypeApi $api;

    const INBOUND_TYPE = 1;
    const OUTBOUND_TYPE = 2;
    const REDIRECT_TYPE = 3;

    public function __construct(StatClickTypeApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getStatClickTypeItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        return [];
    }

    public static function getModelName(): string
    {
        return StatClickType::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/stat_click_types';
    }
}