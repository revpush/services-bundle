<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\PartnerChannelParamApi;
use Swagger\Client\Model\PartnerChannelParamPartnerChannelParamRead;

class PartnerChannelParamApiRepository extends AbstractApiRepository
{
    public const PARTNER_CHANNEL_ID_FILTER = 'partner_channel';

    private PartnerChannelParamApi $api;

    public function __construct(PartnerChannelParamApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    /**
     * @param int $channelId
     * @return PartnerChannelParamPartnerChannelParamRead[]
     * @throws \Swagger\Client\ApiException
     */
    public function findByChannelId(int $channelId): array
    {
        return $this->findBy([self::PARTNER_CHANNEL_ID_FILTER => $channelId]);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getPartnerChannelParamItem($id);
    }

    public static function getModelName(): string
    {
        return PartnerChannelParamPartnerChannelParamRead::class;
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getPartnerChannelParamCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getPartnerChannelParamCollection(...$arguments);
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/partner_channel_params';
    }
}