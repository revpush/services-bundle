<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Proxy\AbstractProxyItem;
use RevPush\ServicesBundle\ApiManager\Proxy\CollectionProxyItem;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\PartnerApi;
use Swagger\Client\Model\AccountAccountRead;
use Swagger\Client\Model\ModelInterface;
use Swagger\Client\Model\PartnerPartnerRead;

class PartnerApiRepository extends AbstractApiRepository
{
    private PartnerApi $api;

    public const SHORT_NAME_FILTER = 'short_name';
    public const PARTNER_TYPE_ID_FILTER = 'partner_type';
    public const GID_FILTER = 'gid';

    const DEFAULT_SOURCE_PARTNER_ID = 15;
    const DEFAULT_SOURCE_PARTNER_NAME = 'ORG';

    public const BUYING_PARTNER_TYPE = 1;
    public const SEARCH_PARTNER_TYPE = 2;

    public function __construct(PartnerApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?ModelInterface
    {
        return $this->api->getPartnerItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getPartnerCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getPartnerCollection(...$arguments);
    }

    /**
     * @param PartnerPartnerRead $partner
     * @return AccountAccountRead|null
     */
    public function getFirstAccountForPartner($partner)
    {
        $accounts = $partner->getAccounts();
        if($accounts instanceof CollectionProxyItem) {
            return $accounts->first();
        }

        return null;
    }

    public static function getModelName(): string
    {
        return PartnerPartnerRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/partners';
    }

    public function getIdentifierFields(): array
    {
        return ['gid', 'short_name'];
    }

    public function getIdentifierFieldsForCollection(): array
    {
        return [
            'accounts' => ['id' => 'partner'],
        ];
    }
}