<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\KeywordApi;
use Swagger\Client\Model\KeywordKeywordRead;

class KeywordApiRepository extends AbstractApiRepository
{
    private KeywordApi $api;

    public function __construct(KeywordApi $api, ApiManager $apiManager, int $cacheTime = self::DEFAULT_CACHE_TIME)
    {
        $this->api = $api;

        parent::__construct($apiManager, $cacheTime);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getKeywordItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getKeywordCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getKeywordCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return KeywordKeywordRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/keywords';
    }
}