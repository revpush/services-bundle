<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\UserTeamApi;
use Swagger\Client\Model\UserTeamUserTeamRead;

class UserTeamApiRepository extends AbstractApiRepository
{
    private UserTeamApi $api;

    public function __construct(UserTeamApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getUserTeamItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getUserTeamCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getUserTeamCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return UserTeamUserTeamRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/user_teams';
    }
}