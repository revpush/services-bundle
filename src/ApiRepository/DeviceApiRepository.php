<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\DeviceApi;
use Swagger\Client\Model\DeviceDeviceRead;

class DeviceApiRepository extends AbstractApiRepository
{
    private DeviceApi $api;

    public const SHORT_NAME_FILTER = 'short_name';

    public function __construct(DeviceApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getDeviceItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getDeviceCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getDeviceCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return DeviceDeviceRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/devices';
    }

    public function getIdentifierFields(): array
    {
        return ['short_name'];
    }
}