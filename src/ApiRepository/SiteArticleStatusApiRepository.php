<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteArticleStatusApi;
use Swagger\Client\Model\SiteArticleStatusSiteArticleStatusRead;

class SiteArticleStatusApiRepository extends AbstractApiRepository
{
    private SiteArticleStatusApi $api;

    public const STATUS_FRAFT_GID = 'draft';
    public const STATUS_PUBLISHED_GID = 'published';

    public function __construct(SiteArticleStatusApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getSiteArticleStatusItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSiteArticleStatusCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteArticleStatusCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteArticleStatusSiteArticleStatusRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_article_statuses';
    }
}