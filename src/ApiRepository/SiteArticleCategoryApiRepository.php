<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteArticleCategoryApi;
use Swagger\Client\Model\SiteArticleCategorySiteArticleCategoryRead;

class SiteArticleCategoryApiRepository extends AbstractApiRepository
{
    private SiteArticleCategoryApi $api;

    public const SITE_ID_FILTER = 'site';
    public const NAMES_FILTER = 'name';
    public const CATEGORY_NAMES_FILTER = 'categoryName';

    public function __construct(SiteArticleCategoryApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    /**
     * @param int $siteId
     * @return SiteArticleCategorySiteArticleCategoryRead[]
     */
    public function findBySite(int $siteId): array
    {
        return $this->findBy([self::SITE_ID_FILTER => $siteId]);
    }

    public function findOneBySite(int $siteId): SiteArticleCategorySiteArticleCategoryRead
    {
        return $this->findOneBy([self::SITE_ID_FILTER => $siteId]);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getSiteArticleCategoryItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSiteArticleCategoryCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteArticleCategoryCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteArticleCategorySiteArticleCategoryRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_article_categories';
    }
}