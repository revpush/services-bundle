<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Proxy\CollectionProxyItem;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\AccessTokenApi;
use Swagger\Client\Model\AccessTokenAccessTokenRead;
use Swagger\Client\Model\AccountAccountRead;
use Swagger\Client\Model\ModelInterface;

class AccessTokenApiRepository extends AbstractApiRepository
{
    private AccessTokenApi $api;
    private const CACHE_OVERTIME = 500;

    public function __construct(AccessTokenApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?ModelInterface
    {
        return $this->api->getAccessTokenItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getAccessTokenCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getAccessTokenCollection(...$arguments);
    }

    /**
     * @param AccountAccountRead $account
     * @return AccessTokenAccessTokenRead|null
     */
    public function getAccessTokenForAccount($account): ?object
    {
        $accessTokens = $account->getAccessTokens();
        if($accessTokens instanceof CollectionProxyItem) {
            return $accessTokens->first();
        }

        return null;
    }

    /**
     * @param AccountAccountRead $account
     * @return AccessTokenAccessTokenRead
     * @throws \Exception
     */
    public function getAccessTokenForAccountOrFail($account): object
    {
        if ($token = $this->getAccessTokenForAccount($account)) {
            return $token;
        }

        throw new \Exception(
            sprintf('Access token is empty for account ID: %d, name: "%s"', $account->getId(), $account->getName())
        );
    }

    public function getCacheTimeByLifetime(object $accessToken): int
    {
        if (is_null($accessToken->getLifetimeToken())) {
            return $this->getCacheTime();
        }

        $diff = (new \DateTimeImmutable())->getTimestamp() - $accessToken->getUpdatedAt()->getTimestamp();
        $cacheTime = (int)$accessToken->getLifetimeToken() - $diff - self::CACHE_OVERTIME;
        if ($cacheTime < 0) {
            $cacheTime = 0;
        }

        return $cacheTime;
    }


    public static function getModelName(): string
    {
        return AccessTokenAccessTokenRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/access_tokens';
    }

    public function getIdentifierFieldsForCollection(): array
    {
        return [
            'access_token_settings' => ['id' => 'access_token']
        ];
    }
}