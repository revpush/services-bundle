<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\PartnerChannelApi;
use Swagger\Client\Model\PartnerChannelPartnerChannelRead;

class PartnerChannelApiRepository extends AbstractApiRepository
{
    private PartnerChannelApi $api;

    public const PARTNER_FILTER = 'partner';
    public const ADGROUP_FILTER = 'adgroup';
    public const KEYWORD_FILTER = 'keyword';

    public function __construct(PartnerChannelApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getPartnerChannelItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getPartnerChannelCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getPartnerChannelCollection(...$arguments);
    }

    /**
     * @param int $adgroupId
     * @param int $partnerId
     * @return PartnerChannelPartnerChannelRead[]
     */
    public function findByAdgroupAndPartner(int $adgroupId, int $partnerId): array
    {
        return $this->findBy([self::ADGROUP_FILTER => $adgroupId, self::PARTNER_FILTER => $partnerId]);
    }

    public function findOneByAdgroupAndPartner(int $adgroupId, int $partnerId): ?PartnerChannelPartnerChannelRead
    {
        return $this->findOneBy([self::ADGROUP_FILTER => $adgroupId, self::PARTNER_FILTER => $partnerId]);
    }

    public function findOneByKeywordAndPartner(int $keywordId, int $partnerId): ?PartnerChannelPartnerChannelRead
    {
        return $this->findOneBy([self::KEYWORD_FILTER => $keywordId, self::PARTNER_FILTER => $partnerId]);
    }

    public static function getModelName(): string
    {
        return PartnerChannelPartnerChannelRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/partner_channels';
    }
}