<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\RequestCheckApi;
use Swagger\Client\Model\RequestCheck;
use Swagger\Client\Model\RequestCheckRequestCheckArgs;
use Swagger\Client\Model\RequestCheckRequestCheckRead;

class RequestCheckApiRepository extends AbstractApiRepository
{
    private RequestCheckApi $api;

    public function __construct(RequestCheckApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        throw new \RuntimeException('Not supported yet');
    }

    protected function getCollection(array $criteria): array
    {
        throw new \RuntimeException('Not supported yet');
    }

    public function post(RequestCheckRequestCheckArgs $args): RequestCheck
    {
        return $this->api->apiRequestChecksPost($args);
    }

    public static function getModelName(): string
    {
        return RequestCheck::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/request_checks';
    }

    public function getCacheTime(): int
    {
        return 0;
    }
}