<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\LanguageApi;
use Swagger\Client\Model\LanguageLanguageRead;
use Swagger\Client\Model\ModelInterface;

class LanguageApiRepository extends AbstractApiRepository
{
    private LanguageApi $api;

    public function __construct(LanguageApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?ModelInterface
    {
        return $this->api->getLanguageItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        return $this->api->getLanguageCollection(...$criteria);
    }

    public static function getModelName(): string
    {
        return LanguageLanguageRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/languages';
    }

    public function getIdentifierFields(): array
    {
        return ['gid'];
    }


}