<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\AdgroupApi;
use Swagger\Client\Model\AdgroupAdgroupRead;

class AdgroupApiRepository extends AbstractApiRepository
{
    private AdgroupApi $api;

    public const LINK_KEY_FILTER = 'link_key';

    public function __construct(AdgroupApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getAdgroupItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getAdgroupCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getAdgroupCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return AdgroupAdgroupRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/adgroups';
    }

    public function getIdentifierFields(): array
    {
        return ['link_key'];
    }

    public function getIdentifierFieldsForCollection(): array
    {
        return [
            'keywords' => ['id' => 'adgroup']
        ];
    }

    public function getRelationRepositories(): array
    {
        return [
            'search_partner' => PartnerApiRepository::class,
        ];
    }
    
    public function findByLinkKey(?string $linkKey): ?AdgroupAdgroupRead
    {
        if ($linkKey === null) {
            return null;
        }

        if (empty($linkKey = trim($linkKey))) {
            return null;
        }

        return $this->findOneBy([self::LINK_KEY_FILTER => $linkKey]);
    }
}