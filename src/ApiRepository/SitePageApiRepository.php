<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SitePageApi;
use Swagger\Client\Model\SitePageSitePageRead;

class SitePageApiRepository extends AbstractApiRepository
{
    private SitePageApi $api;

    public const SITE_ID_FILTER = 'site';

    public function __construct(SitePageApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getSitePageItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSitePageCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSitePageCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SitePageSitePageRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_pages';
    }
}