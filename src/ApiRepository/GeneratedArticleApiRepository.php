<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\GeneratedArticleApi;
use Swagger\Client\Model\GeneratedArticleJsonldGeneratedArticleRead;
use Swagger\Client\Model\GetGeneratedArticleCollection200Response;

class GeneratedArticleApiRepository extends AbstractApiRepository
{
    public const string KEYWORD_FILTER = 'keyword';
    public const string STATUS_FILTER = 'status';

    public const string STATUS_GENERATED = 'generated';

    public function __construct(
        private GeneratedArticleApi $api,
        ApiManager $apiManager
    )
    {
        parent::__construct($apiManager);
    }

    public function findGeneratedArticlesByKeyword(int $keywordId, int $limit, int $page): GetGeneratedArticleCollection200Response
    {
        return $this->findByWithMeta([
            self::KEYWORD_FILTER => $keywordId,
            self::STATUS_FILTER => self::STATUS_GENERATED,
            self::LIMIT_FILTER => $limit,
            self::PAGE_FILTER => $page,
        ]);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getGeneratedArticleItem($id);
    }    protected function getCollection(array $criteria): array
{
    return $this->getCollectionWithMeta($criteria)->getHydramember();
}

    /**
     * @param array $criteria
     * @return object|GetGeneratedArticleCollection200Response
     * @throws \RevPush\ServicesBundle\ApiManager\Exception\CriteriaException
     * @throws \Swagger\Client\ApiException
     */
    protected function getCollectionWithMeta(array $criteria): object
    {
        $method = 'getGeneratedArticleCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getGeneratedArticleCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return GeneratedArticleJsonldGeneratedArticleRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/generated_articles';
    }
}