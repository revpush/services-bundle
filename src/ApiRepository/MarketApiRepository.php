<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\MarketApi;
use Swagger\Client\Model\MarketMarketRead;
use Swagger\Client\Model\ModelInterface;

class MarketApiRepository extends AbstractApiRepository
{
    private MarketApi $api;

    public function __construct(MarketApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }
    
    protected function getItemById(int $id): ?object
    {
        return $this->api->getMarketItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getMarketCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getMarketCollection(...$arguments);
    }

    public function getMarketByCountryCode(string $countryCode): ?object
    {
        $markets = $this->findAll();
        foreach($markets as $market) {
            if($market->getGid() === $countryCode) {
                return $market;
            }
        }

        return null;
    }

    public static function getModelName(): string
    {
        return MarketMarketRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/markets';
    }

    public function getIdentifierFields(): array
    {
        return ['gid'];
    }
}