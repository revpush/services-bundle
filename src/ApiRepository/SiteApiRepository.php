<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteApi;
use Swagger\Client\Model\ModelInterface;
use Swagger\Client\Model\SiteSiteRead;

class SiteApiRepository extends AbstractApiRepository
{
    private SiteApi $api;

    public const DOMAIN_FILTER = 'domain';
    public const SITE_PAGE_NAME_FILTER = 'site_pages_name';
    public const LINK_FORMAT_PARAM_NAME_FILTER = 'link_format_param_sites_link_format_param_name';

    public const MMG_SITE_GROUP_GID = 'mmg';
    public const THIRD_PARTY_SITE_GROUP_GID = 'third_party';
    public const SYSTEM_SITE_GROUP_GID = 'system';

    public function __construct(SiteApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?ModelInterface
    {
        return $this->api->getSiteItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSiteCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteSiteRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/sites';
    }

    public function getIdentifierFields(): array
    {
        return ['domain'];
    }

    public function getIdentifierFieldsForCollection(): array
    {
        return [
            'feed_zones' => ['id' => 'site'],
            'site_pages' => ['id' => 'site'],
            'link_format_param_sites' => ['id' => 'site'],
        ];
    }
}