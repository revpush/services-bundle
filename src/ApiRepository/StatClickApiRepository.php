<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\StatClickApi;
use Swagger\Client\Model\StatClickStatClickRead;

class StatClickApiRepository
{
    private StatClickApi $api;

    public function __construct(StatClickApi $api)
    {
        $this->api = $api;
    }

    public function save(StatClickStatClickRead $statClick): void
    {
        $this->api->postStatClickCollection($statClick);
    }

    public static function getModelName(): string
    {
        return StatClickStatClickRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        return '/api/stat_clicks';
    }
}