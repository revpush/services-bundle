<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Proxy\CollectionProxyItem;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\AccountApi;
use Swagger\Client\Model\AccessTokenAccessTokenRead;
use Swagger\Client\Model\AccountAccountRead;
use Swagger\Client\Model\ModelInterface;

class AccountApiRepository extends AbstractApiRepository
{
    private AccountApi $api;

    public const PARTNER_ID_FILTER = 'partner';

    public function __construct(AccountApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?ModelInterface
    {
        return $this->api->getAccountItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getAccountCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getAccountCollection(...$arguments);
    }

    /**
     * @param AccountAccountRead $account
     * @return AccessTokenAccessTokenRead|null
     */
    public function getAccessTokenForAccount($account): ?object
    {
        $accessTokens = $account->getAccessTokens();
        if($accessTokens instanceof CollectionProxyItem) {
            return $accessTokens->first();
        }

        return null;
    }

    public static function getModelName(): string
    {
        return AccountAccountRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/accounts';
    }

    public function getIdentifierFieldsForCollection(): array
    {
        return [
            'access_tokens' => ['id' => 'account']
        ];
    }
}