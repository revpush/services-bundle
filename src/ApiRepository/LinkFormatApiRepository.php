<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\LinkFormatApi;
use Swagger\Client\Model\LinkFormatLinkFormatRead;

class LinkFormatApiRepository extends AbstractApiRepository
{
    private LinkFormatApi $api;

    public function __construct(LinkFormatApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getLinkFormatItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getLinkFormatCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getLinkFormatCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return LinkFormatLinkFormatRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/link_formats';
    }
}