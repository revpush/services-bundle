<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteArticleTemplateApi;
use Swagger\Client\Model\SiteArticleTemplateSiteArticleTemplateRead;

class SiteArticleTemplateApiRepository extends AbstractApiRepository
{
    private SiteArticleTemplateApi $api;

    public const SITE_ID_FILTER = 'site';

    public function __construct(SiteArticleTemplateApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }
    
    public function findBySite(int $siteId)
    {
        return $this->findBy([self::SITE_ID_FILTER => $siteId]);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getSiteArticleTemplateItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSiteArticleTemplateCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteArticleTemplateCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteArticleTemplateSiteArticleTemplateRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_article_templates';
    }
}