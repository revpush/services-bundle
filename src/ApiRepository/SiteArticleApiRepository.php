<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteArticleApi;
use Swagger\Client\Model\GetSiteArticleCollection200Response;
use Swagger\Client\Model\SiteArticleJsonldSiteArticleRead;

class SiteArticleApiRepository extends AbstractApiRepository
{
    private SiteArticleApi $api;

    public const SITE_ID_FILTER = 'site';
    public const SLUG_FILTER = 'slug';
    public const TITLE_FILTER = 'title';
    public const SITE_ARTICLE_CATEGORY_ID_FILTER = 'category';
    public const SITE_ARTICLE_CATEGORY_NAME_FILTER = 'category_name';
    public const SITE_ARTICLE_STATUS_ID_FILTER = 'status';
    public const SITE_ARTICLE_STATUS_GID_FILTER = 'status_gid';
    public const ORDER_CREATED_AT = 'order_created_at';
    public const ORDER_UPDATED_AT = 'order_updated_at';
    public const ORDER_SLUG = 'order_slug';
    public const ORDER_TITLE = 'order_title';

    public function __construct(SiteArticleApi $api, ApiManager $apiManager)
    {
        $this->api = $api;
        parent::__construct($apiManager);
    }

    public function findPublishedBySiteWithMeta(int $siteId, int $limit, int $page): GetSiteArticleCollection200Response
    {
        return $this->findByWithMeta([
            self::SITE_ARTICLE_STATUS_GID_FILTER => SiteArticleStatusApiRepository::STATUS_PUBLISHED_GID,
            self::SITE_ID_FILTER => $siteId,
            self::LIMIT_FILTER => $limit,
            self::PAGE_FILTER => $page,
        ]);
    }

    public function findOnePublishedBySiteWithMeta(int $siteId): ?SiteArticleJsonldSiteArticleRead
    {
        return $this->findOneBy([
            self::SITE_ARTICLE_STATUS_GID_FILTER => SiteArticleStatusApiRepository::STATUS_PUBLISHED_GID,
            self::SITE_ID_FILTER => $siteId,
            self::LIMIT_FILTER => 1,
            self::PAGE_FILTER => 1,
        ]);
    }

    /**
     * @param int $id
     * @return object|\Swagger\Client\Model\SiteArticleJsonldSiteArticleRead|null
     * @throws \Swagger\Client\ApiException
     */
    protected function getItemById(int $id): ?object
    {
        return $this->api->getSiteArticleItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        return $this->getCollectionWithMeta($criteria)->getHydramember();
    }

    /**
     * @param array $criteria
     * @return object|GetSiteArticleCollection200Response
     * @throws \RevPush\ServicesBundle\ApiManager\Exception\CriteriaException
     * @throws \Swagger\Client\ApiException
     */
    protected function getCollectionWithMeta(array $criteria): object
    {
        $method = 'getSiteArticleCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteArticleCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteArticleJsonldSiteArticleRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_articles';
    }
}