<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\SiteArticleImageApi;
use Swagger\Client\Model\SiteArticleImageSiteArticleImageRead;

class SiteArticleImageApiRepository extends AbstractApiRepository
{
    private SiteArticleImageApi $api;

    public function __construct(SiteArticleImageApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getSiteArticleImageItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getSiteArticleImageCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getSiteArticleImageCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return SiteArticleImageSiteArticleImageRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/site_article_images';
    }
}