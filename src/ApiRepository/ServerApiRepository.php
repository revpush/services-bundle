<?php

namespace RevPush\ServicesBundle\ApiRepository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Repository\AbstractApiRepository;
use Swagger\Client\Api\ServerApi;
use Swagger\Client\Model\ServerServerRead;

class ServerApiRepository extends AbstractApiRepository
{
    private ServerApi $api;

    public function __construct(ServerApi $api, ApiManager $apiManager)
    {
        $this->api = $api;

        parent::__construct($apiManager);
    }

    protected function getItemById(int $id): ?object
    {
        return $this->api->getServerItem($id);
    }

    protected function getCollection(array $criteria): array
    {
        $method = 'getServerCollection';
        $arguments = $this->getArguments(get_class($this->api), $method, $criteria);
        return $this->api->getServerCollection(...$arguments);
    }

    public static function getModelName(): string
    {
        return ServerServerRead::class;
    }

    public static function getResourcePath(?int $id = null): string
    {
        if(!is_null($id)) {
            return parent::getResourcePathWithId($id);
        }

        return '/api/servers';
    }
}