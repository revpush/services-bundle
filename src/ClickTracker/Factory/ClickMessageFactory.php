<?php

namespace RevPush\ServicesBundle\ClickTracker\Factory;

use RevPush\ServicesBundle\ApiFactory\StatClickFactory;
use RevPush\ServicesBundle\ApiRepository\StatClickTypeApiRepository;
use RevPush\ServicesBundle\ClickTracker\Message\InboundClickMessage;
use RevPush\ServicesBundle\ClickTracker\Message\OutboundClickMessage;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

readonly class ClickMessageFactory
{
    public function __construct(
        private StatClickFactory $statClickFactory
    ) {}

    public function createInboundClickMessage(UserParamsCollection $userParamsCollection): ?InboundClickMessage
    {
        if($statClick = $this->statClickFactory->createFromUserParamsCollection($userParamsCollection, StatClickTypeApiRepository::INBOUND_TYPE)) {
            return new InboundClickMessage($statClick);
        }

        return null;
    }

    public function createOutboundClickMessage(UserParamsCollection $userParamsCollection): ?OutboundClickMessage
    {
        if($statClick = $this->statClickFactory->createFromUserParamsCollection($userParamsCollection, StatClickTypeApiRepository::OUTBOUND_TYPE)) {
            return new OutboundClickMessage($statClick);
        }

        return null;
    }
}