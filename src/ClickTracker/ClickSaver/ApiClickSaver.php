<?php

namespace RevPush\ServicesBundle\ClickTracker\ClickSaver;

use Psr\Log\LoggerInterface;
use RevPush\ServicesBundle\ApiFactory\StatClickFactory;
use RevPush\ServicesBundle\ApiRepository\AdgroupApiRepository;
use RevPush\ServicesBundle\ApiRepository\StatClickApiRepository;
use RevPush\ServicesBundle\ApiRepository\StatClickTypeApiRepository;
use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

readonly class ApiClickSaver implements ClickSaverInterface
{
    public function __construct(
        private StatClickApiRepository $statClickApiRepository,
        private LoggerInterface $logger,
        private AdgroupApiRepository $adgroupApiRepository,
        private StatClickFactory $statClickFactory
    ) {}

    public function saveInboundClick(UserParamsCollection $userParamsCollection): void
    {
        try {
            $linkKey = $userParamsCollection->getLinkFormatParams()->getValue(Param::LINK_KEY);
            if (($adgroup = $this->adgroupApiRepository->findByLinkKey($linkKey)) && $adgroup->getUseRedirect()) {
                return;
            }

            if ($statClick = $this->statClickFactory->createFromUserParamsCollection($userParamsCollection, StatClickTypeApiRepository::INBOUND_TYPE)) {
                $this->statClickApiRepository->save($statClick);
            }
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
        }
    }

    public function saveOutboundClick(UserParamsCollection $userParamsCollection): void
    {
        try {
            if ($statClick = $this->statClickFactory->createFromUserParamsCollection($userParamsCollection, StatClickTypeApiRepository::OUTBOUND_TYPE)) {
                $this->statClickApiRepository->save($statClick);
            }
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
        }
    }
}