<?php

namespace RevPush\ServicesBundle\ClickTracker\ClickSaver;

use RevPush\ServicesBundle\UserParams\UserParamsCollection;

interface ClickSaverInterface
{
    public function saveInboundClick(UserParamsCollection $userParamsCollection): void;
    public function saveOutboundClick(UserParamsCollection $userParamsCollection): void;
}