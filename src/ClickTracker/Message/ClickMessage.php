<?php

namespace RevPush\ServicesBundle\ClickTracker\Message;

use RevPush\ServicesBundle\Message\ApiMessageInterface;
use Swagger\Client\Model\StatClickStatClickRead;

class ClickMessage implements ApiMessageInterface
{
    private StatClickStatClickRead $statClickDto;

    protected string $class;

    public function __construct(StatClickStatClickRead $statClickDto)
    {
        $this->statClickDto = $statClickDto;
        $this->class = get_class($statClickDto);
    }

    public function getStatClickDto(): StatClickStatClickRead
    {
        return $this->statClickDto;
    }

    public function getClass(): string
    {
        return $this->class;
    }
}