<?php

namespace RevPush\ServicesBundle\ClickTracker\Message;

use RevPush\ServicesBundle\Message\AsyncHighPriorityInterface;

class OutboundClickMessage extends ClickMessage implements AsyncHighPriorityInterface
{

}