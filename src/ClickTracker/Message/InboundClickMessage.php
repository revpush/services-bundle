<?php

namespace RevPush\ServicesBundle\ClickTracker\Message;

use RevPush\ServicesBundle\Message\AsyncNormalPriorityInterface;

class InboundClickMessage extends ClickMessage implements AsyncNormalPriorityInterface
{

}