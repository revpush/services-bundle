<?php


namespace RevPush\ServicesBundle\GeoIp;


interface GeoProviderInterface
{
    public function setGeoData(GeoDto $geoDto): void;
}