<?php


namespace RevPush\ServicesBundle\GeoIp;


use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZippopotamGeoProvider implements GeoProviderInterface
{
    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function setGeoData(GeoDto $geoDto): void
    {
        if(!$geoDto->market) {
            throw new \InvalidArgumentException('Market is not set');
        }

        if(!$geoDto->zipCode) {
            throw new \InvalidArgumentException('ZipCode is not set');
        }

        $response = $this->httpClient->request(
            'GET',
            'http://api.zippopotam.us/' . $geoDto->market . '/' . $geoDto->zipCode
        );

        if($content = $response->toArray()) {
            if(isset($content['places'][0]['state abbreviation'])) {
                $geoDto->state = $content['places'][0]['state abbreviation'];
            }

            if(isset($content['places'][0]['place name'])) {
                $geoDto->city = $content['places'][0]['place name'];
            }
        }
    }
}