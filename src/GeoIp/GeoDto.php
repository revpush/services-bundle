<?php


namespace RevPush\ServicesBundle\GeoIp;


class GeoDto
{
    public $ip;
    public $city;
    public $countryCode;
    public $market;
    public $languageCode;
    public $state;
    public $zipCode;
    public $latitude;
    public $longitude;
}