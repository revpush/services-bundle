<?php


namespace RevPush\ServicesBundle\GeoIp;


use GeoIp2\Database\Reader;

class MaxMindGeoProvider implements GeoProviderInterface
{
    private string $dir;
    private string $geoFile;
    private ?Reader $geoReader = null;

    public function __construct(string $dir, string $geoFile)
    {
        $this->dir = $dir;
        $this->geoFile = $geoFile;
    }

    public function setGeoData(GeoDto $geoDto): void
    {
        if(!$geoDto->ip) {
            throw new \InvalidArgumentException('Ip is not set');
        }

        $reader = $this->createGeoReader();

        if($cityRecord = $reader->city($geoDto->ip)) {
            if(isset($cityRecord->city->names['en'])) {
                $geoDto->city = $cityRecord->city->names['en'];
            }

            if(isset($cityRecord->country->isoCode)) {
                $geoDto->countryCode = $cityRecord->country->isoCode;
            }

            if(isset($cityRecord->mostSpecificSubdivision->isoCode)) {
                $geoDto->state = $cityRecord->mostSpecificSubdivision->isoCode;
            }

            if(isset($cityRecord->postal->code)) {
                $geoDto->zipCode = $cityRecord->postal->code;
            }

            if(isset($cityRecord->locales) && !empty($cityRecord->locales)) {
                $geoDto->languageCode = $cityRecord->locales[0];
            }

            $latitude = $cityRecord->location->latitude;
            $longitude = $cityRecord->location->longitude;
            if(isset($latitude, $longitude)) {
                $geoDto->latitude = $latitude;
                $geoDto->longitude = $longitude;
            }

        }
    }

    private function createGeoReader(): Reader
    {
        if(!$this->geoReader) {
            $this->geoReader = new Reader($this->dir . '/' . $this->geoFile);
        }

        return $this->geoReader;
    }
}