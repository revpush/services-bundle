<?php

namespace RevPush\ServicesBundle\ApiManager\Exception;

class IdentifierFieldException extends ApiManagerException
{
    public static function notValidIdentifierField(string $identifierField, string $modelName): self
    {
        return new self(sprintf(
            'Identifier field "%s" is not valid for "%s" model',
            $identifierField,
            $modelName
        ));
    }

    public static function emptyIdentifierFields(string $model): self
    {
        return new self(sprintf(
            'Empty identifier fields for "%s" model',
            $model
        ));
    }

    public static function notFoundIdentifierFieldForCollection(string $identifierField, string $repositoryClass): self
    {
        return new self(sprintf(
            'Identifier field "%s" is not found in "%s" repository',
            $identifierField,
            $repositoryClass
        ));
    }

    public static function emptyIdentifierValue(string $modelClass, string $identifierField): self
    {
        return new self(sprintf(
            'Identifier value for model "%s" and field "%s" is empty',
            $modelClass,
            $identifierField
        ));
    }
}