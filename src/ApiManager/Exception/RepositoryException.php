<?php

namespace RevPush\ServicesBundle\ApiManager\Exception;

class RepositoryException extends ApiManagerException
{
    public static function noRepoNameFound(string $repositoryName): self
    {
        return new self(sprintf(
            'Repository "%s" is not found',
            $repositoryName
        ));
    }

    public static function noModelNameFound(string $modelName): self
    {
        return new self(sprintf(
            'Repository is not found by model name "%s"',
            $modelName
        ));
    }
}