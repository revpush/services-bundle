<?php

namespace RevPush\ServicesBundle\ApiManager\Exception;

class ModelException extends ApiManagerException
{
    public static function create(string $modelName): self
    {
        return new self(sprintf(
            'Class "%s" is not a model',
            $modelName
        ));
    }
}