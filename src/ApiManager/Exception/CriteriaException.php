<?php

namespace RevPush\ServicesBundle\ApiManager\Exception;

class CriteriaException extends ApiManagerException
{
    public static function notValidCriteriaValue(string $argument, string $className, string $method): self
    {
        return new self(sprintf(
            'Invalid "%s" criteria value. "%s::%s"',
            $argument,
            $className,
            $method
        ));
    }

    public static function notValidCriteriaName(string $criteriaField): self
    {
        return new self(sprintf(
            'Invalid criteria field "%s"',
            $criteriaField
        ));
    }

    public static function notValidCriteriaType(string $criteriaField, string $criteriaType, string $expectedType, string $className, string $methodName): self
    {
        return new self(sprintf(
            'Invalid criteria type. Expected type "%s" given "%s". For %s::"%s"',
            $expectedType,
            $criteriaType,
            $className,
            $methodName
        ));
    }
}