<?php

namespace RevPush\ServicesBundle\ApiManager\Exception;

class MetadataException extends ApiManagerException
{
    public static function notValidModelType(): self
    {
        return new self('Type of the model should be string or object');
    }

    public static function notFoundMetadata(string $modelClass): self
    {
        return new self(sprintf(
            'Metadata for "%s" class is not found',
            $modelClass
        ));
    }

    public static function notFoundField(string $field, string $modelClass): self
    {
        return new self(sprintf(
            'Field "%s" is not found for "%s" model',
            $field,
            $modelClass
        ));
    }

    public static function notFoundGetter(string $field, string $modelClass): self
    {
        return new self(sprintf(
            'Getter is not found for field "%s" in "%s" model',
            $field,
            $modelClass
        ));
    }

    public static function notFoundSetter(string $field, string $modelClass): self
    {
        return new self(sprintf(
            'Setter is not found for field "%s" in "%s" model',
            $field,
            $modelClass
        ));
    }

    public static function notFoundIdentifierFieldsForCollection(string $field, string $repositoryClass): self
    {
        return new self(sprintf(
            'Identifier fields for collection "%s" are not found in "%s" repository',
            $field,
            $repositoryClass
        ));
    }
}