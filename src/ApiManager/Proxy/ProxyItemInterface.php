<?php

namespace RevPush\ServicesBundle\ApiManager\Proxy;

interface ProxyItemInterface
{
    public function initialize(): void;
}