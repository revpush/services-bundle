<?php

namespace RevPush\ServicesBundle\ApiManager\Proxy;

use RevPush\ServicesBundle\ApiManager\Exception\MetadataException;
use RevPush\ServicesBundle\ApiManager\Metadata\FieldRelationMetadata;
use RevPush\ServicesBundle\ApiManager\Metadata\ModelMetadata;
use RevPush\ServicesBundle\ApiManager\Repository\ApiRepositoryCollection;

class ProxyFactory implements ProxyFactoryInterface
{
    private ApiRepositoryCollection $repositoryCollection;

    public function __construct(ApiRepositoryCollection $repositoryCollection)
    {
        $this->repositoryCollection = $repositoryCollection;
    }

    public function create($parentModel, ModelMetadata $modelMetadata, FieldRelationMetadata $fieldMetadata, $relationFieldValue): ProxyItemInterface
    {
        if($fieldMetadata->isCollection()) {
            $proxyItem = $this->createCollectionProxyItem($parentModel, $modelMetadata, $fieldMetadata, $relationFieldValue);
        } else {
            $proxyItem = $this->createObjectProxyItem($fieldMetadata, $relationFieldValue);
        }

        return $proxyItem;
    }

    private function createCollectionProxyItem($parentModel, ModelMetadata $modelMetadata, FieldRelationMetadata $fieldMetadata, $relationFieldValue): ProxyItemInterface
    {
        $relationRepository = $this->repositoryCollection->get($fieldMetadata->getRelationRepositoryClass());
        if($collectionIdentifierField = $fieldMetadata->getCollectionIdentifierField()) {
            if(!$relationField = $modelMetadata->getField($collectionIdentifierField->getRelationField())) {
                throw MetadataException::notFoundField($collectionIdentifierField->getRelationField(), $modelMetadata->getModelClass());
            }

            if(!$getter = $relationField->getGetter()) {
                throw MetadataException::notFoundGetter($relationField->getName(), $modelMetadata->getModelClass());
            }

            $collectionIdentifierFieldValue = $parentModel->$getter();
            $collectionLoader = static function () use ($relationRepository, $collectionIdentifierField, $collectionIdentifierFieldValue) {
                return $relationRepository->findBy([$collectionIdentifierField->getCriteriaName() => $collectionIdentifierFieldValue]);
            };
        } else {
            $collectionLoader = function () use ($fieldMetadata, $relationFieldValue) {
                return array_map(function ($item) use ($fieldMetadata, $relationFieldValue) {
                    return $this->createObjectProxyItem($fieldMetadata, $item);
                }, $relationFieldValue);
            };
        }

        return new CollectionProxyItem($collectionLoader, $relationFieldValue);
    }

    private function createObjectProxyItem(FieldRelationMetadata $fieldMetadata, $relationFieldValue): ProxyItemInterface
    {
        $valueLoader = null;
        $dependencyResourceIri = null;
        $dependencyId = null;

        if($fieldMetadata->getRelationRepositoryClass()) {
            $relationRepository = $this->repositoryCollection->get($fieldMetadata->getRelationRepositoryClass());

            if(is_string($relationFieldValue)) {
                $dependencyResourceIri = $relationFieldValue;
                if($dependencyId = $this->getDependencyId($relationFieldValue, $relationRepository::getResourcePath())) {
                    $valueLoader = static function () use ($dependencyId, $relationRepository) {
                        return $relationRepository->find($dependencyId);
                    };
                }
            } elseif(is_object($relationFieldValue)) {
                if(method_exists($relationFieldValue, 'getId')) {
                    $dependencyId = $relationFieldValue->getId();
                    $dependencyResourceIri = $relationRepository::getResourcePath($dependencyId);
                    $valueLoader = static function () use ($dependencyId, $relationRepository) {
                        return $relationRepository->find($dependencyId);
                    };
                }
            }
        }

        if(is_null($valueLoader)) {
            $valueLoader = static function () use ($relationFieldValue) {
                return $relationFieldValue;
            };
        }

        return new ObjectProxyItem($valueLoader, $relationFieldValue, $dependencyId);
    }

    private function getDependencyId(string $resourceIri, string $resourcePath): int
    {
        return (int) trim(substr($resourceIri, strlen($resourcePath)), '/');
    }
}