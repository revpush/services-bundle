<?php

namespace RevPush\ServicesBundle\ApiManager\Proxy;

abstract class AbstractProxyItem implements ProxyItemInterface
{
    protected \Closure $initializer;
    protected bool $initialized = false;
    protected $receivedValue;
    protected $initializedValue;

    public function __construct(\Closure $initializer, $receivedValue)
    {
        $this->initializer = $initializer;
        $this->receivedValue = $receivedValue;
    }

    public function __invoke(): ?object
    {
        $this->initialize();
        return $this->getInitializedValue();
    }

    public function initialize(): void
    {
        if(!$this->initialized) {
            $initializer = $this->initializer;
            $this->initializedValue = $initializer();
            $this->initialized = true;
        }
    }

    protected function getInitializedValue()
    {
        return $this->initializedValue;
    }

    public function getReceivedValue()
    {
        return $this->receivedValue;
    }
}