<?php

namespace RevPush\ServicesBundle\ApiManager\Proxy;

class ObjectProxyItem extends AbstractProxyItem
{
    private ?int $valueId;

    public function __construct(\Closure $initializer, $receivedValue, ?int $valueId = null)
    {
        parent::__construct($initializer, $receivedValue);
        $this->valueId = $valueId;
    }

    public function __call($method, $arguments)
    {
        $this->initialize();
        if($model = $this->getInitializedValue()) {
            return $model->$method(...$arguments);
        }

        return null;
    }

    public function __toString(): string
    {
        if(is_string($this->getReceivedValue())) {
            return $this->getReceivedValue();
        }

        if(is_object($this->getReceivedValue())) {
            return json_encode($this->getReceivedValue());
        }

        return '';
    }

    public function getId(): ?int
    {
        return $this->valueId;
    }
}