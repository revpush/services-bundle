<?php

namespace RevPush\ServicesBundle\ApiManager\Proxy;

class CollectionProxyItem extends AbstractProxyItem implements \IteratorAggregate, \Countable, \ArrayAccess
{
    protected $initializedValue = [];

    public function getIterator(): \ArrayIterator
    {
        $this->initialize();
        $initializedValue = is_array($this->getInitializedValue()) ? $this->getInitializedValue() : [];
        return new \ArrayIterator($initializedValue);
    }

    public function __toString(): string
    {
        if(is_array($this->getReceivedValue())) {
            $collection = [];
            foreach($this->getReceivedValue() as $value) {
                if($value instanceof AbstractProxyItem || is_string($value)) {
                    $collection[] = (string) $value;
                } else {
                    $collection[] = $value;
                }
            }

            return json_encode($collection);
        }

        return '';
    }

    public function count(): int
    {
        if(is_countable($this->getReceivedValue())) {
            return count($this->getReceivedValue());
        }

        return 0;
    }

    public function first(): ?object
    {
        $this->initialize();

        if(is_iterable($this->initializedValue) && count($this->initializedValue) > 0) {
            return $this->initializedValue[0];
        }

        return null;
    }

    /**
     * @param $offset
     * @param $value
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        $this->initialize();
        if (is_null($offset)) {
            $this->initializedValue[] = $value;
        } else {
            $this->initializedValue[$offset] = $value;
        }
    }

    public function offsetExists($offset): bool
    {
        $this->initialize();
        return isset($this->initializedValue[$offset]);
    }

    public function offsetUnset($offset): void
    {
        $this->initialize();
        if ($this->offsetExists($offset)) {
            unset($this->initializedValue[$offset]);
        }
    }

    /**
     * @param $offset
     * @return mixed|null
     */
    public function offsetGet($offset): mixed
    {
        $this->initialize();
        return $this->offsetExists($offset) ? $this->initializedValue[$offset] : null;
    }
}