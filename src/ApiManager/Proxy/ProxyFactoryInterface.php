<?php

namespace RevPush\ServicesBundle\ApiManager\Proxy;

use RevPush\ServicesBundle\ApiManager\Metadata\FieldMetadata;
use RevPush\ServicesBundle\ApiManager\Metadata\ModelMetadata;
use RevPush\ServicesBundle\ApiManager\Metadata\FieldRelationMetadata;

interface ProxyFactoryInterface
{
    public function create($parentModel, ModelMetadata $modelMetadata, FieldRelationMetadata $fieldMetadata, $relationFieldValue): ProxyItemInterface;
}