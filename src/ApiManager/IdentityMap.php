<?php

namespace RevPush\ServicesBundle\ApiManager;

class IdentityMap
{
    private array $objects = [];

    public function add(object $model, string $identifierField, $identifierValue): void
    {
        $this->objects[get_class($model)][$identifierField][$identifierValue] = $model;
    }

    public function get(string $modelClass, string $identifierField, $identifierValue): ?object
    {
        if ($this->has($modelClass, $identifierField, $identifierValue) === false) {
            return null;
        }

        return $this->objects[$modelClass][$identifierField][$identifierValue];
    }

    public function has(string $modelClass, string $identifierField, $identifierValue): bool
    {
        if (!isset($this->objects[$modelClass]) ||
            !isset($this->objects[$modelClass][$identifierField]) ||
            !isset($this->objects[$modelClass][$identifierField][$identifierValue])) {
            return false;
        }
        
        return true;
    }

    public function clear(): void
    {
        $this->objects = [];
    }
}