<?php

namespace RevPush\ServicesBundle\ApiManager\Repository;

use RevPush\ServicesBundle\ApiManager\ApiManager;
use RevPush\ServicesBundle\ApiManager\Exception\CriteriaException;

abstract class AbstractApiRepository implements ApiRepositoryInterface
{
    public const LIMIT_FILTER = 'limit';
    public const PAGE_FILTER = 'page';

    protected const DEFAULT_CACHE_TIME = 600;

    private ApiManager $apiManager;

    protected int $cacheTime;

    abstract protected function getItemById(int $id): ?object;
    abstract protected function getCollection(array $criteria): array;

    public function __construct(ApiManager $apiManager, int $cacheTime = self::DEFAULT_CACHE_TIME)
    {
        $this->apiManager = $apiManager;
        $this->cacheTime = $cacheTime;
    }

    public function find(int $id): ?object
    {
        $valueLoader = function () use ($id) {
            return $this->getItemById($id);
        };

        return $this->getApiManager()->find($valueLoader, $this::getModelName(), $id, $this->getCacheTime());
    }

    public function findBy(array $criteria): array
    {
        $valueLoader = function () use ($criteria) {
            return $this->getCollection($criteria);
        };

        return $this->getApiManager()->findBy($valueLoader, $this::getModelName(), $criteria, $this->getCacheTime());
    }

    public function findByWithMeta(array $criteria): object
    {
        $valueLoader = function () use ($criteria) {
            return $this->getCollectionWithMeta($criteria);
        };

        return $this->getApiManager()->findByWithMeta($valueLoader, $this::getModelName(), $criteria, $this->getCacheTime());
    }

    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function findAllWithMeta(): object
    {
        return $this->findByWithMeta([]);
    }

    public function findOneBy(array $criteria): ?object
    {
        $valueLoader = function () use ($criteria) {
            return $this->getCollection($criteria);
        };
        
        return $this->getApiManager()->findOneBy($valueLoader, $this::getModelName(), $criteria, $this->getCacheTime());
    }

    public function getIdentifierFields(): array
    {
        return [];
    }

    /**
     * Key is field name of current model
     * Nested key is identifier field of current model
     * Nested value is criteria name in api
     *
     * @return array
     */
    public function getIdentifierFieldsForCollection(): array
    {
        return [];
    }

    public function getRelationRepositories(): array
    {
        return [];
    }

    public static function getResourcePathWithId(?int $id = null): string
    {
        return rtrim(static::getResourcePath(), '/') . '/' . $id;
    }

    public function setCacheTime(int $cacheTime): void
    {
        $this->cacheTime = $cacheTime;
    }

    public function getCacheTime(): int
    {
        return $this->cacheTime;
    }

    protected function getCollectionWithMeta(array $criteria): object
    {
        throw new \RuntimeException('method ' . __METHOD__ . ' is not implemented');
    }

    protected function getArguments(string $className, string $methodName, array $criteria): array
    {
        $arguments = [];
        $reflectionMethod = new \ReflectionMethod($className, $methodName);
        $params = $reflectionMethod->getParameters();

        $this->checkCriteria($params, $criteria);

        foreach ($params as $param) {
            try {
                $argument = $criteria[$param->getName()] ?? $param->getDefaultValue();
            } catch (\Exception $exception) {
                throw CriteriaException::notValidCriteriaValue($param->getName(), $className, $methodName);
            }

            $arguments[] = $argument;
        }

        return $arguments;
    }

    protected function getApiManager(): ApiManager
    {
        return $this->apiManager;
    }

    private function checkCriteria(array $reflectionParameters, array $criteria): void
    {
        foreach($criteria as $field => $value) {
            $isValidField = false;
            foreach($reflectionParameters as $reflectionParameter) {
                if($field === $reflectionParameter->getName()) {
                    $isValidField = true;
                }
            }

            if(!$isValidField) {
                throw CriteriaException::notValidCriteriaName($field);
            }
        }
    }
}