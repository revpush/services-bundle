<?php

namespace RevPush\ServicesBundle\ApiManager\Repository;

interface ApiRepositoryInterface
{
    public function find(int $id): ?object;
    public function findAll(): array;
    public function findAllWithMeta(): object;
    public function findBy(array $criteria): array;
    public function findByWithMeta(array $criteria): object;
    public function findOneBy(array $criteria): ?object;
    public static function getModelName(): string;
    public static function getResourcePath(?int $id = null): string;
    public function getIdentifierFields(): array;
    public function getIdentifierFieldsForCollection(): array;
    public function getRelationRepositories(): array;
    public function setCacheTime(int $cacheTime): void;
    public function getCacheTime(): int;
}