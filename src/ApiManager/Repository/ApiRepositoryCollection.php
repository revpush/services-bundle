<?php

namespace RevPush\ServicesBundle\ApiManager\Repository;

use RevPush\ServicesBundle\ApiManager\Exception\RepositoryException;

class ApiRepositoryCollection
{
    /** @var ApiRepositoryInterface[] */
    private array $repositories = [];
    /** @var ApiRepositoryInterface[] */
    private array $modelRepositories = [];

    public function has(string $repositoryName): bool
    {
        return isset($this->repositories[$repositoryName]);
    }

    /**
     * @throws RepositoryException
     */
    public function get(string $repositoryName): ApiRepositoryInterface
    {
        if(!$this->has($repositoryName)) {
            throw RepositoryException::noRepoNameFound($repositoryName);
        }

        return $this->repositories[$repositoryName];
    }

    public function getByModel(string $modelName): ApiRepositoryInterface
    {
        if(!isset($this->modelRepositories[$modelName])) {
            throw RepositoryException::noModelNameFound($modelName);
        }

        return $this->modelRepositories[$modelName];
    }

    public function set(ApiRepositoryInterface $objectRepository, string $className): self
    {
        $this->repositories[$className] = $objectRepository;
        $this->modelRepositories[$objectRepository::getModelName()] = $objectRepository;

        return $this;
    }

    /**
     * @return ApiRepositoryInterface[]
     */
    public function getAll(): array
    {
        return $this->repositories;
    }
}