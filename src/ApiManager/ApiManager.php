<?php

namespace RevPush\ServicesBundle\ApiManager;

use RevPush\ServicesBundle\ApiManager\Exception\ApiManagerException;
use RevPush\ServicesBundle\ApiManager\Metadata\ModelMetadataService;
use RevPush\ServicesBundle\ApiManager\Proxy\ProxyFactoryInterface;
use RevPush\ServicesBundle\ApiManager\Repository\ApiRepositoryCollection;
use RevPush\ServicesBundle\ApiManager\Repository\ApiRepositoryInterface;

class ApiManager
{
    private ApiRepositoryCollection $repositoryCollection;
    private ModelMetadataService $modelMetadataService;
    private ProxyFactoryInterface $proxyFactory;
    private UnitOfWork $unitOfWork;
    private SecondLevelCache $secondLevelCache;

    public function __construct(
        ApiRepositoryCollection $repositoryCollection,
        ModelMetadataService $modelMetadataService,
        ProxyFactoryInterface $proxyFactory,
        UnitOfWork $unitOfWork,
        SecondLevelCache $secondLevelCache
    )
    {
        $this->repositoryCollection = $repositoryCollection;
        $this->modelMetadataService = $modelMetadataService;
        $this->unitOfWork = $unitOfWork;
        $this->proxyFactory = $proxyFactory;
        $this->secondLevelCache = $secondLevelCache;
    }

    public function getRepository(string $repositoryName): ApiRepositoryInterface
    {
        return $this->repositoryCollection->get($repositoryName);
    }

    public function getUnitOfWork(): UnitOfWork
    {
        return $this->unitOfWork;
    }

    public function find(\Closure $valueLoader, string $modelClass, int $id, int $cacheTime = 0): ?object
    {
        $object = $this->load($valueLoader, $modelClass, ['id' => $id], $cacheTime);

        if (is_object($object)) {
            $this->initializeLoaded($object);
            return $object;
        }

        return null;
    }

    public function findBy(\Closure $valueLoader, string $modelClass, array $criteria, int $cacheTime = 0): array
    {
        $collection = $this->load($valueLoader, $modelClass, $criteria, $cacheTime);

        if (is_iterable($collection)) {
            foreach ($collection as $object) {
                $this->initializeLoaded($object);
            }
        } else {
            $collection = [];
        }

        return $collection;
    }

    public function findByWithMeta(\Closure $valueLoader, string $modelClass, array $criteria, int $cacheTime = 0): object
    {
        $collection = $this->load($valueLoader, $modelClass, $criteria, $cacheTime);

        if (!is_object($collection)) {
            throw new ApiManagerException('Returned result of collection must be an object');
        }

        if (method_exists($collection, 'getHydramember')) {
            foreach ($collection->getHydramember() ?? [] as $object) {
                $this->initializeLoaded($object);
            }
        }

        return $collection;
    }

    public function findOneBy(\Closure $valueLoader, string $modelClass, array $criteria, int $cacheTime = 0): ?object
    {
        $data = $this->load($valueLoader, $modelClass, $criteria, $cacheTime);

        if (is_object($data)) {
            $this->initializeLoaded($data);
            return $data;
        }

        if (is_iterable($data)) {
            foreach ($data as $object) {
                $this->initializeLoaded($object);
                return $object;
            }
        }

        return null;
    }

    private function load(\Closure $valueLoader, string $modelClass, array $criteria, int $cacheTime = 0)
    {
        if ($value = $this->getUnitOfWork()->getFromIdentityMap($modelClass, $criteria)) {
            return $value;
        }

        if ($cacheTime > 0) {
            $value = $this->secondLevelCache->getFromCache($valueLoader, $modelClass, $criteria, $cacheTime);
        } else {
            $value = $valueLoader();
        }

        return $value;
    }

    private function initializeLoaded(object $object): void
    {
        $this->createProxyObjectsForRelations($object);

        if ($this->getUnitOfWork()->isInIdentityMap($object) === false) {
            $this->getUnitOfWork()->addToIdentityMap($object);
        }
    }

    private function createProxyObjectsForRelations(object $model): void
    {
        if(!$modelMetadata = $this->modelMetadataService->getMetadataForObject($model)) {
            return;
        }

        foreach ($modelMetadata->getRelationFields() as $fieldMetadata) {
            $getter = $fieldMetadata->getGetter();
            $relationFieldValue = $model->$getter();

            if (is_null($relationFieldValue) || is_object($relationFieldValue)) {
                continue;
            }

            $proxyItem = $this->proxyFactory->create($model, $modelMetadata, $fieldMetadata, $relationFieldValue);

            $setter = $fieldMetadata->getSetter();
            $model->$setter($proxyItem);
        }
    }
}