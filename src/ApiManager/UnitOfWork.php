<?php

namespace RevPush\ServicesBundle\ApiManager;

use RevPush\ServicesBundle\ApiManager\Exception\IdentifierFieldException;
use RevPush\ServicesBundle\ApiManager\Exception\MetadataException;
use RevPush\ServicesBundle\ApiManager\Metadata\ModelMetadataService;

class UnitOfWork
{
    private IdentityMap $identityMap;
    private ModelMetadataService $modelMetadataService;

    public function __construct(
        ModelMetadataService $modelMetadataService
    )
    {
        $this->identityMap = new IdentityMap();
        $this->modelMetadataService = $modelMetadataService;
    }

    public function isInIdentityMap(object $model): bool
    {
        if(!$metadata = $this->modelMetadataService->getMetadataForObject($model)) {
            return false;
        }

        foreach ($metadata->getIdentifierFields() as $identifierField) {
            if (!$getter = $identifierField->getGetter()) {
                throw MetadataException::notFoundGetter($identifierField->getName(), $model);
            }

            if (false === ($identifierValue = $model->$getter())) {
                throw IdentifierFieldException::emptyIdentifierValue(get_class($model), $identifierField->getName());
            }

            if ($this->identityMap->has($metadata->getModelClass(), $identifierField->getName(), $identifierValue)) {
                return true;
            }
        }
        
        return false;
    }

    public function getFromIdentityMap(string $modelClass, array $criteria = []): ?object
    {
        if(!$metadata = $this->modelMetadataService->getMetadataForClass($modelClass)) {
            return null;
        }

        foreach ($criteria as $field => $value) {
            if ($metadata->isFieldIdentifier($field)) {
                if ($object = $this->identityMap->get($modelClass, $field, $value)) {
                    return $object;
                }
            }
        }

        return null;
    }

    /**
     * @param object $model
     * @return void
     * @throws IdentifierFieldException
     * @throws MetadataException
     */
    public function addToIdentityMap(object $model): void
    {
        if(!$metadata = $this->modelMetadataService->getMetadataForObject($model)) {
            return;
        }

        foreach ($metadata->getIdentifierFields() as $identifierField) {
            if (!$getter = $identifierField->getGetter()) {
                throw MetadataException::notFoundGetter($identifierField->getName(), $model);
            }

            if (false === ($identifierValue = $model->$getter())) {
                throw IdentifierFieldException::emptyIdentifierValue(get_class($model), $identifierField->getName());
            }

            $this->identityMap->add($model, $identifierField->getName(), $identifierValue);
        }

    }

    public function clearIdentityMap(): void
    {
        $this->identityMap->clear();
    }
}