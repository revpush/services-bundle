<?php

namespace RevPush\ServicesBundle\ApiManager;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Swagger\Client\ApiException;

class SecondLevelCache
{
    private CacheItemPoolInterface $cache;
    private LoggerInterface $logger;

    private const string SHORT_CACHE_TYPE = 'short';
    private const string LONG_CACHE_TYPE = 'long';
    private const string SHORT_CACHE_ERROR = 'short_err';

    private int $longCacheTime = 86400;
    private int $shortCacheErrorTime = 15;
    private array $failedRequestCodes = [
        0, // request timeout
        500, // api internal error
    ];

    public function __construct(
        CacheItemPoolInterface $cache,
        LoggerInterface $logger
    )
    {
        $this->cache = $cache;
        $this->logger = $logger;
    }

    public function getFromCache(\Closure $valueLoader, string $modelClass, array $criteria, int $cacheTime)
    {
        $value = null;
        $shortCachedItem = $this->getCacheItem($this->generateCacheKey($modelClass, $criteria, self::SHORT_CACHE_TYPE));
        if (!$shortCachedItem) {
            return null;
        }

        if ($shortCachedItem->isHit()) {
            return $shortCachedItem->get();
        }

        $longCachedItem = $this->getCacheItem($this->generateCacheKey($modelClass, $criteria, self::LONG_CACHE_TYPE));
        $shortCacheError = $this->getCacheItem(self::SHORT_CACHE_ERROR);

        if ($shortCacheError && $shortCacheError->isHit()) {
            if ($longCachedItem && $longCachedItem->isHit()) {
                return $longCachedItem->get();
            }
        } else {
            try {

                $value = $valueLoader();

                $shortCachedItem->expiresAfter($cacheTime);
                $shortCachedItem->set($value);
                $this->cache->saveDeferred($shortCachedItem);

                $longCachedItem->expiresAfter($this->longCacheTime);
                $longCachedItem->set($value);
                $this->cache->saveDeferred($longCachedItem);

                $this->cache->commit();

            } catch (\Throwable $exception) {
                $this->handleValueLoaderException($exception, $shortCacheError);

                if ($longCachedItem->isHit()) {
                    $value = $longCachedItem->get();
                }
            }
        }

        return $value;
    }

    private function getCacheItem(string $key): ?CacheItemInterface
    {
        try {
            return $this->cache->getItem($key);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
        }

        return null;
    }

    private function generateCacheKey(string $modelClass, array $criteria, string $type): string
    {
        return $type . '_' . sha1($modelClass . '_' . serialize($criteria));
    }

    private function handleValueLoaderException(\Throwable $exception, CacheItemInterface $shortCacheError): void
    {
        $this->logger->error($exception);

        if ($exception instanceof ApiException && in_array((int)$exception->getCode(), $this->failedRequestCodes, true)) {
            try {
                $shortCacheError->expiresAfter($this->shortCacheErrorTime);
                $shortCacheError->set(true);
                $this->cache->saveDeferred($shortCacheError);
                $this->cache->commit();
            } catch (\Throwable $exception) {
                $this->logger->error($exception);
            }
        }
    }
}