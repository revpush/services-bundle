<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

class FieldRelationMetadata extends FieldMetadata
{
    private string $getter;
    private string $setter;
    private string $relationModelClass;
    private string $relationRepositoryClass;

    public function __construct(string $name, string $getter, string $setter, string $relationModelClass, string $relationRepositoryClass)
    {
        $this->getter = $getter;
        $this->setter = $setter;
        $this->relationModelClass = $relationModelClass;
        $this->relationRepositoryClass = $relationRepositoryClass;

        parent::__construct($name);
    }

    public function getGetter(): string
    {
        return $this->getter;
    }

    public function getSetter(): string
    {
        return $this->setter;
    }

    public function getRelationModelClass(): string
    {
        return $this->relationModelClass;
    }

    public function getRelationRepositoryClass(): string
    {
        return $this->relationRepositoryClass;
    }
}