<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

use RevPush\ServicesBundle\ApiManager\Exception\MetadataException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class ModelMetadataService
{
    private MetadataCollectionFactoryInterface $metaDataCollectionFactory;
    private ?MetadataCollection $metaDataCollection = null;
    private CacheInterface $cache;
    private int $cacheTime;

    private bool $initialized = false;

    public function __construct(
        MetadataCollectionFactoryInterface $metaDataCollectionFactory,
        CacheInterface $cache,
        int $cacheTime = 1800
    )
    {
        $this->metaDataCollectionFactory = $metaDataCollectionFactory;
        $this->cache = $cache;
        $this->cacheTime = $cacheTime;
    }

    public function getMetadataForClass(string $modelClass): ?ModelMetadata
    {
        $this->loadCollection();
        return $this->metaDataCollection->get($modelClass);
    }

    public function getMetadataForObject(object $model): ?ModelMetadata
    {
        if($metadata = $this->getMetadataForClass(get_class($model))) {
            $this->metaDataCollectionFactory->build($model, $metadata);

            return $metadata;
        }

        return null;
    }

    private function loadCollection()
    {
        if(!$this->initialized) {
            $collection = null;
            if($this->cacheTime > 0) {
                $collection = $this->cache->get('model_metadata_collection', function (ItemInterface $item) {
                    $item->expiresAfter($this->cacheTime);
                    return $this->metaDataCollectionFactory->create();
                });
            }

            if(is_null($collection)) {
                $collection = $this->metaDataCollectionFactory->create();
            }

            $this->metaDataCollection = $collection;
            $this->initialized = true;
        }
    }

    public function getCacheTime(): int
    {
        return $this->cacheTime;
    }

    public function setCacheTime(int $cacheTime): ModelMetadataService
    {
        $this->cacheTime = $cacheTime;
        return $this;
    }
}