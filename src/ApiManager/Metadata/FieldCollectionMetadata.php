<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

class FieldCollectionMetadata extends FieldRelationMetadata
{
    private ?CollectionIdentifierField $collectionIdentifierField;

    public function __construct(string $name, string $getter, string $setter, string $relationModelClass, string $relationRepositoryClass, ?CollectionIdentifierField $collectionIdentifierField = null)
    {
        $this->collectionIdentifierField = $collectionIdentifierField;

        parent::__construct($name, $getter, $setter, $relationModelClass, $relationRepositoryClass);
    }

    public function getCollectionIdentifierField(): ?CollectionIdentifierField
    {
        return $this->collectionIdentifierField;
    }
}