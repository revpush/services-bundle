<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

class FieldMetadata
{
    private string $name;
    private ?string $getter = null;
    private ?string $setter = null;
    private bool $isCollection = false;
    private ?CollectionIdentifierField $collectionIdentifierField = null;
    private bool $isIdentifier = false;
    private bool $isRelation = false;
    private ?string $relationModelClass = null;
    private ?string $relationRepositoryClass = null;


    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FieldMetadata
     */
    public function setName(string $name): FieldMetadata
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGetter(): ?string
    {
        return $this->getter;
    }

    /**
     * @param string|null $getter
     * @return FieldMetadata
     */
    public function setGetter(?string $getter): FieldMetadata
    {
        $this->getter = $getter;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSetter(): ?string
    {
        return $this->setter;
    }

    /**
     * @param string|null $setter
     * @return FieldMetadata
     */
    public function setSetter(?string $setter): FieldMetadata
    {
        $this->setter = $setter;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCollection(): bool
    {
        return $this->isCollection;
    }

    /**
     * @param bool $isCollection
     * @return FieldMetadata
     */
    public function setIsCollection(bool $isCollection): FieldMetadata
    {
        $this->isCollection = $isCollection;
        return $this;
    }

    /**
     * @return CollectionIdentifierField|null
     */
    public function getCollectionIdentifierField(): ?CollectionIdentifierField
    {
        return $this->collectionIdentifierField;
    }

    /**
     * @param CollectionIdentifierField|null $collectionIdentifierField
     * @return FieldMetadata
     */
    public function setCollectionIdentifierField(?CollectionIdentifierField $collectionIdentifierField): FieldMetadata
    {
        $this->collectionIdentifierField = $collectionIdentifierField;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIdentifier(): bool
    {
        return $this->isIdentifier;
    }

    /**
     * @param bool $isIdentifier
     * @return FieldMetadata
     */
    public function setIsIdentifier(bool $isIdentifier): FieldMetadata
    {
        $this->isIdentifier = $isIdentifier;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRelation(): bool
    {
        return $this->isRelation;
    }

    /**
     * @param bool $isRelation
     * @return FieldMetadata
     */
    public function setIsRelation(bool $isRelation): FieldMetadata
    {
        $this->isRelation = $isRelation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRelationModelClass(): ?string
    {
        return $this->relationModelClass;
    }

    /**
     * @param string|null $relationModelClass
     * @return FieldMetadata
     */
    public function setRelationModelClass(?string $relationModelClass): FieldMetadata
    {
        $this->relationModelClass = $relationModelClass;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRelationRepositoryClass(): ?string
    {
        return $this->relationRepositoryClass;
    }

    /**
     * @param string|null $relationRepositoryClass
     * @return FieldMetadata
     */
    public function setRelationRepositoryClass(?string $relationRepositoryClass): FieldMetadata
    {
        $this->relationRepositoryClass = $relationRepositoryClass;
        return $this;
    }
}