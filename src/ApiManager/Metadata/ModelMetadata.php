<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

class ModelMetadata
{
    private string $modelClass;
    private string $repositoryClass;
    /** @var FieldMetadata[] */
    private array $fields = [];
    /** @var FieldMetadata[] */
    private array $identifierFields = [];
    /** @var FieldRelationMetadata[] */
    private array $relationFields = [];

    public function __construct(string $modelClass, string $repositoryClass)
    {
        $this->modelClass = $modelClass;
        $this->repositoryClass = $repositoryClass;
    }

    public function getModelClass(): string
    {
        return $this->modelClass;
    }

    public function getRepositoryClass(): string
    {
        return $this->repositoryClass;
    }

    public function getField(string $field): ?FieldMetadata
    {
        return $this->fields[$field] ?? null;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function setFields(array $fields): ModelMetadata
    {
        $this->fields = $fields;
        return $this;
    }

    public function isFieldIdentifier(string $field): bool
    {
        return isset($this->identifierFields[$field]);
    }

    public function getIdentifierFields(): array
    {
        return $this->identifierFields;
    }

    public function setIdentifierFields(array $identifierFields): ModelMetadata
    {
        $this->identifierFields = $identifierFields;
        return $this;
    }

    public function isFieldRelation(string $field): bool
    {
        return isset($this->relationFields[$field]);
    }

    public function getRelationFields(): array
    {
        return $this->relationFields;
    }

    public function setRelationFields(array $relationFields): ModelMetadata
    {
        $this->relationFields = $relationFields;
        return $this;
    }
}