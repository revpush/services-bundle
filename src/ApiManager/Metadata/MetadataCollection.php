<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

class MetadataCollection
{
    private array $metadata = [];

    public function get(string $modelClass): ?ModelMetadata
    {
        if($metadata = $this->metadata[$modelClass] ?? null) {
            return clone $metadata;
        }

        return null;
    }

    public function set(ModelMetadata $modelMetadata): void
    {
        $this->metadata[$modelMetadata->getModelClass()] = $modelMetadata;
    }

    public function getAll(): array
    {
        return $this->metadata;
    }
}