<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata\Factory;

use RevPush\ServicesBundle\ApiManager\Exception\MetadataException;
use RevPush\ServicesBundle\ApiManager\Metadata\CollectionIdentifierField;
use RevPush\ServicesBundle\ApiManager\Metadata\FieldCollectionMetadata;
use RevPush\ServicesBundle\ApiManager\Metadata\FieldMetadata;
use RevPush\ServicesBundle\ApiManager\Metadata\FieldRelationMetadata;
use RevPush\ServicesBundle\ApiManager\Repository\ApiRepositoryCollection;
use RevPush\ServicesBundle\ApiManager\Exception\ModelException;
use RevPush\ServicesBundle\ApiManager\Metadata\MetadataCollection;
use RevPush\ServicesBundle\ApiManager\Metadata\MetadataCollectionFactoryInterface;
use RevPush\ServicesBundle\ApiManager\Metadata\ModelMetadata;
use RevPush\ServicesBundle\ApiManager\Repository\ApiRepositoryInterface;
use Swagger\Client\Model\ModelInterface;

class SwaggerMetadataCollectionFactory implements MetadataCollectionFactoryInterface
{
    private ApiRepositoryCollection $repositoryCollection;

    public function __construct(
        ApiRepositoryCollection $repositoryCollection
    )
    {
        $this->repositoryCollection = $repositoryCollection;
    }

    public function create(): MetadataCollection
    {
        $collection = new MetadataCollection();
        foreach ($this->repositoryCollection->getAll() as $repositoryClass => $repository) {
            if (is_a($repository::getModelName(), ModelInterface::class, true) === false) {
                throw ModelException::create($repository::getModelName());
            }

            $metaData = new ModelMetadata($repository::getModelName(), $repositoryClass);
            $fieldsMetadata = $this->createFieldsMetadata($repository::getModelName());
            $metaData->setFields($fieldsMetadata);

            $identifierFields = [];
            $relationFields = [];
            /** @var FieldMetadata $field */
            foreach ($fieldsMetadata as $field) {
                if ($field->isIdentifier()) {
                    $identifierFields[$field->getName()] = $field;
                }

                if ($field->isRelation()) {
                    $relationFields[$field->getName()] = $field;
                }
            }

            $metaData->setIdentifierFields($identifierFields);
            $metaData->setRelationFields($relationFields);

            $collection->set($metaData);
        }

        return $collection;
    }

    public function build(object $model, ModelMetadata $modelMetadata): void
    {
        foreach ($modelMetadata->getRelationFields() as $fieldMetadata) {
            if (!$fieldMetadata->getRelationRepositoryClass() || !$fieldMetadata->getRelationModelClass()) {
                if ($getter = $fieldMetadata->getGetter()) {
                    $resourceIri = $model->$getter();

                    if ($fieldMetadata->isCollection()) {
                        $resourceIri = $resourceIri[array_key_first($resourceIri)];
                    }

                    if (is_string($resourceIri)) {
                        foreach ($this->repositoryCollection->getAll() as $repositoryClass => $repository) {
                            if (strpos($resourceIri, $repository::getResourcePath()) !== false) {
                                $fieldMetadata->setRelationRepositoryClass($repositoryClass);
                                $fieldMetadata->setRelationModelClass($repository::getModelName());
                            }
                        }
                    }
                }
            }
        }
    }

    protected function createFieldsMetadata(string $modelClass): array
    {
        $fields = [];

        /** @var ModelInterface $modelClass */
        $getters = $modelClass::getters();
        $setters = $modelClass::setters();
        $swaggerTypes = $modelClass::openAPITypes();
        $swaggerFormats = $modelClass::openAPIFormats();
        $modelRepository = $this->repositoryCollection->getByModel($modelClass);
        $identifiers = array_merge(['id'], $modelRepository->getIdentifierFields());
        $relationRepositories = $modelRepository->getRelationRepositories();

        foreach ($modelClass::attributeMap() as $fieldName => $originalName) {
            $relationRepositoryClass = null;
            $relationRepository = null;
            $getter = $getters[$fieldName] ?? null;
            $setter = $setters[$fieldName] ?? null;
            $swaggerType = $swaggerTypes[$fieldName] ?? null;
            $swaggerFormat = $swaggerFormats[$fieldName] ?? null;
            $isCollection = $this->isFieldCollection($swaggerType);
            if($isRelation = $this->isFieldRelation($swaggerType, $swaggerFormat)) {
                $relationRepositoryClass = $relationRepositories[$fieldName] ?? null;
                if (!$relationRepositoryClass) {
                    $relationRepositoryClass = $this->getRelationRepositoryClass($fieldName, $isCollection);
                }
                if($this->repositoryCollection->has($relationRepositoryClass)) {
                    $relationRepository = $this->repositoryCollection->get($relationRepositoryClass);
                } else {
                    $isRelation = false;
                }
            }

            $field = null;
            if ($isRelation) {
                $relationModelClass = $relationRepository::getModelName();

                if ($isCollection) {
                    $collectionIdentifierField = $this->createCollectionIdentifierField($modelRepository, $fieldName);
                    $field = new FieldCollectionMetadata($fieldName, $getter, $setter, $relationModelClass, $relationRepositoryClass, $collectionIdentifierField);
                } else {
                    $field = new FieldRelationMetadata($fieldName, $getter, $setter, $relationModelClass, $relationRepositoryClass);
                }
            } else {
                $field = (new FieldMetadata($fieldName))
                    ->setGetter($getter)
                    ->setSetter($setter);
            }

            $field
                ->setIsRelation($isRelation)
                ->setIsCollection($isCollection);

            if (in_array($fieldName, $identifiers)) {
                $field->setIsIdentifier(true);
            }

            $fields[$fieldName] = $field;
        }

        return $fields;
    }

    protected function getRelationRepositoryClass(string $field, bool $plural = false): string
    {
        $field = $plural ? rtrim($field, 's') : $field;
        $repositoryName = str_replace('_', '', ucwords($field, '_')) . 'ApiRepository';
        return 'RevPush\\ServicesBundle\\ApiRepository\\' . $repositoryName;
    }

    private function isFieldCollection(?string $swaggerType): bool
    {
        if (str_contains($swaggerType, '[]')) {
            return true;
        }

        return false;
    }

    private function isFieldRelation(?string $swaggerType, ?string $swaggerFormat): bool
    {
        if ($swaggerFormat === 'iri-reference') {
            return true;
        }

        if (str_contains($swaggerType, '\\')) {
            $swaggerTypeNamespace = str_replace('[]', '', $swaggerType);
            if (class_exists($swaggerTypeNamespace) && is_a($swaggerTypeNamespace, ModelInterface::class, true)) {
                return true;
            }
        }

        return false;
    }

    private function createCollectionIdentifierField(ApiRepositoryInterface $repository, string $fieldName): ?CollectionIdentifierField
    {
        $identifierFieldsForCollection = $repository->getIdentifierFieldsForCollection();
        if ($identifierFieldCollection = $identifierFieldsForCollection[$fieldName] ?? null) {
            $relationField = key($identifierFieldCollection);
            $criteriaName = $identifierFieldCollection[$relationField];
            return new CollectionIdentifierField($relationField, $criteriaName);
        }

        return null;
    }
}