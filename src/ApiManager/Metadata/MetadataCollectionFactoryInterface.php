<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

interface MetadataCollectionFactoryInterface
{
    public function create(): MetadataCollection;
    public function build(object $model, ModelMetadata $modelMetadata): void;
}