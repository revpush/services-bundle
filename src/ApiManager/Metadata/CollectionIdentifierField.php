<?php

namespace RevPush\ServicesBundle\ApiManager\Metadata;

class CollectionIdentifierField
{
    private string $relationField;
    private string $criteriaName;

    public function __construct(string $relationField, string $criteriaName)
    {
        $this->relationField = $relationField;
        $this->criteriaName = $criteriaName;
    }

    public function getRelationField(): string
    {
        return $this->relationField;
    }

    public function getCriteriaName(): string
    {
        return $this->criteriaName;
    }
}