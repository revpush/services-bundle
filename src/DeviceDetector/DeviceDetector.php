<?php

namespace RevPush\ServicesBundle\DeviceDetector;

class DeviceDetector
{
    const MOBILE_SHORT_NAME = 'M';
    const DESKTOP_SHORT_NAME = 'D';
    const TABLETS_SHORT_NAME = 'T';

    private DeviceDetectorInterface $deviceDetector;

    public function __construct(
        DeviceDetectorInterface $deviceDetector
    )
    {
        $this->deviceDetector = $deviceDetector;
    }

    /**
     * @return string
     */
    public function getDeviceName(): string
    {
        $device = self::DESKTOP_SHORT_NAME;
        if($this->deviceDetector->isMobile()) {
            $device = self::MOBILE_SHORT_NAME;
        } elseif($this->deviceDetector->isTablet()) {
            $device = self::TABLETS_SHORT_NAME;
        }

        return $device;
    }
}