<?php

namespace RevPush\ServicesBundle\RefererReplacement\IntermediateSite;

class IntermediateSiteCollection
{
    private array $sites = [];

    public function add(IntermediateSite $site): self
    {
        $this->sites[$site->getId()] = $site;
        return $this;
    }

    public function count(): int
    {
        return count($this->sites);
    }

    public function get(int $siteId): ?IntermediateSite
    {
        return $this->sites[$siteId] ?? null;
    }

    public function all(): array
    {
        return $this->sites;
    }
}