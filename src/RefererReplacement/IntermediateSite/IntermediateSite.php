<?php

namespace RevPush\ServicesBundle\RefererReplacement\IntermediateSite;

class IntermediateSite
{
    private int $id;
    private string $domain;

    public function __construct(int $id, string $domain)
    {
        $this->id = $id;
        $this->domain = $domain;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }
}