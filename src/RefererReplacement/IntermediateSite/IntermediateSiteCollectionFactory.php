<?php

namespace RevPush\ServicesBundle\RefererReplacement\IntermediateSite;

use RevPush\ServicesBundle\ApiRepository\SiteApiRepository;
use Swagger\Client\Model\SiteSiteRead;

class IntermediateSiteCollectionFactory implements IntermediateSiteCollectionFactoryInterface
{
    private const PAGE_NAME = IntermediateSitePage::PAGE_NAME;
    private const LINK_FORMAT_PARAM_NAME = '{redirect_page}';

    private SiteApiRepository $siteApiRepository;

    public function __construct(SiteApiRepository $siteApiRepository)
    {
        $this->siteApiRepository = $siteApiRepository;
    }

    public function create(): IntermediateSiteCollection
    {
        $sites = new IntermediateSiteCollection();

        foreach ($this->findApiSites() as $apiSite) {
            $site = $this->createFromApiSite($apiSite);
            $sites->add($site);
        }

        return $sites;
    }

    private function findApiSites()
    {
        return $this->siteApiRepository->findBy([
            SiteApiRepository::SITE_PAGE_NAME_FILTER => self::PAGE_NAME,
            SiteApiRepository::LINK_FORMAT_PARAM_NAME_FILTER => self::LINK_FORMAT_PARAM_NAME,
        ]);
    }

    private function createFromApiSite(SiteSiteRead $apiSite): IntermediateSite
    {
        return new IntermediateSite($apiSite->getId(), $apiSite->getDomain());
    }
}