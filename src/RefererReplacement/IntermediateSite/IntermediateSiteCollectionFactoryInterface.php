<?php

namespace RevPush\ServicesBundle\RefererReplacement\IntermediateSite;

interface IntermediateSiteCollectionFactoryInterface
{
    public function create(): IntermediateSiteCollection;
}