<?php

namespace RevPush\ServicesBundle\RefererReplacement\IntermediateSite;

class IntermediateSitePage
{
    public const PAGE_NAME = 'ClickFilter Redirect';

    private int $id;
    private string $path;
    private IntermediateSite $site;

    public function __construct(int $id, string $path, IntermediateSite $site)
    {
        $this->id = $id;
        $this->path = $path;
        $this->site = $site;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getSite(): IntermediateSite
    {
        return $this->site;
    }
}