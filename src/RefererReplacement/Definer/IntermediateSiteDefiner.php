<?php

namespace RevPush\ServicesBundle\RefererReplacement\Definer;

use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSite;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSiteCollectionFactoryInterface;

class IntermediateSiteDefiner implements IntermediateSiteDefinerInterface
{
    private IntermediateSiteCollectionFactoryInterface $siteCollectionFactory;

    public function __construct(IntermediateSiteCollectionFactoryInterface $siteCollectionFactory)
    {
        $this->siteCollectionFactory = $siteCollectionFactory;
    }

    public function define(array $options = []): ?IntermediateSite
    {
        $siteCollection = $this->siteCollectionFactory->create();
        if ($siteCollection->count() === 0) {
            return null;
        }

        $siteId = array_rand($siteCollection->all());
        return $siteCollection->get($siteId);
    }
}