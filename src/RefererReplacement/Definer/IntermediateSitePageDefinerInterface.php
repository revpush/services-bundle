<?php

namespace RevPush\ServicesBundle\RefererReplacement\Definer;

use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSite;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSitePage;

interface IntermediateSitePageDefinerInterface
{
    public function define(IntermediateSite $site, array $options = []): IntermediateSitePage;
}