<?php

namespace RevPush\ServicesBundle\RefererReplacement\Definer;

use RevPush\ServicesBundle\ApiRepository\SitePageApiRepository;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSite;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSitePage;
use Swagger\Client\Model\SitePageSitePageRead;

class IntermediateSitePageDefiner implements IntermediateSitePageDefinerInterface
{
    private SitePageApiRepository $sitePageApiRepository;

    public function __construct(SitePageApiRepository $sitePageApiRepository)
    {
        $this->sitePageApiRepository = $sitePageApiRepository;
    }

    public function define(IntermediateSite $site, array $options = []): IntermediateSitePage
    {
        $apiSitePages = $this->sitePageApiRepository->findBy([
            SitePageApiRepository::SITE_ID_FILTER => $site->getId()
        ]);

        /** @var SitePageSitePageRead $apiSitePage */
        foreach ($apiSitePages as $apiSitePage) {
            if ($apiSitePage->getName() === IntermediateSitePage::PAGE_NAME) {
                return $this->createFromApiSitePage($apiSitePage, $site);
            }
        }

        throw new \LogicException('Could not define Redirect Site Page for Site ID: ' . $site->getId());
    }
    
    private function createFromApiSitePage(SitePageSitePageRead $apiSitePage, IntermediateSite $site): IntermediateSitePage
    {
        return new IntermediateSitePage($apiSitePage->getId(), $apiSitePage->getPath(), $site);
    }
}