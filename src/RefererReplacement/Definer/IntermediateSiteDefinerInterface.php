<?php

namespace RevPush\ServicesBundle\RefererReplacement\Definer;

use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSite;

interface IntermediateSiteDefinerInterface
{
    public function define(array $options = []): ?IntermediateSite;
}