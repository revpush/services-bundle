<?php

namespace RevPush\ServicesBundle\RefererReplacement\LinkParser;

use RevPush\ServicesBundle\LinkManager\LinkParser\LinkParser;
use RevPush\ServicesBundle\LinkManagerClient\Middleware\RedirectPage;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

class IntermediateSiteLinkParser
{
    private LinkParser $linkParser;

    public function __construct(LinkParser $linkParser)
    {
        $this->linkParser = $linkParser;
    }

    public function parse(string $pageUrl, UserParamsCollection $userParamsCollection): void
    {
        $linkFormatParams = $userParamsCollection->getLinkFormatParams();

        $this->linkParser->withMiddlewares([
            RedirectPage::class,
        ])->parse($pageUrl, $linkFormatParams);
    }
}