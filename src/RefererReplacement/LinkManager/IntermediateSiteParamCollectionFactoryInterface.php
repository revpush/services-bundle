<?php

namespace RevPush\ServicesBundle\RefererReplacement\LinkManager;

use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSitePage;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

interface IntermediateSiteParamCollectionFactoryInterface
{
    public function create(
        IntermediateSitePage $intermediateSitePage,
        UserParamsCollection $currentUserParams,
        string               $targetUrl
    ): ParamsCollection;
}