<?php

namespace RevPush\ServicesBundle\RefererReplacement\LinkManager;

use RevPush\ServicesBundle\LinkManager\Param;
use RevPush\ServicesBundle\LinkManager\ParamsCollection;
use RevPush\ServicesBundle\LinkManager\ParamsCollectionFactoryInterface;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSitePage;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

class IntermediateSiteParamCollectionFactory implements IntermediateSiteParamCollectionFactoryInterface
{
    private ParamsCollectionFactoryInterface $paramsCollectionFactory;

    public function __construct(ParamsCollectionFactoryInterface $paramsCollectionFactory)
    {
        $this->paramsCollectionFactory = $paramsCollectionFactory;
    }

    public function create(
        IntermediateSitePage $intermediateSitePage,
        UserParamsCollection $currentUserParams,
        string               $targetUrl
    ): ParamsCollection
    {
        $keyword = $currentUserParams->getLinkFormatParams()->getValue(Param::KEYWORD);

        $linkFormatParams = $this->paramsCollectionFactory->createCollection();
        $linkFormatParams->setValue(Param::SITE_ID, $intermediateSitePage->getSite()->getId());
        $linkFormatParams->setValue(Param::SITE_DOMAIN, $intermediateSitePage->getSite()->getDomain());
        $linkFormatParams->setValue(Param::SITE_PAGE, $intermediateSitePage->getPath());
        $linkFormatParams->setValue(Param::KEYWORD, $keyword);
        $linkFormatParams->setValue(Param::REDIRECT_PAGE, $targetUrl);

        return $linkFormatParams;
    }
}