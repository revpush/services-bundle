<?php

namespace RevPush\ServicesBundle\RefererReplacement\LinkGenerator;

use Exception;
use RevPush\ServicesBundle\LinkManager\LinkGenerator\LinkGenerator;
use RevPush\ServicesBundle\LinkManagerClient\Middleware\LinkFormatParamSite;
use RevPush\ServicesBundle\LinkManagerClient\Middleware\RedirectPage;
use RevPush\ServicesBundle\LinkManagerClient\Middleware\UrlBuilder;
use RevPush\ServicesBundle\RefererReplacement\IntermediateSite\IntermediateSitePage;
use RevPush\ServicesBundle\RefererReplacement\LinkManager\IntermediateSiteParamCollectionFactoryInterface;
use RevPush\ServicesBundle\UserParams\UserParamsCollection;

class IntermediateSiteLinkGenerator
{
    private LinkGenerator $linkGenerator;
    private IntermediateSiteParamCollectionFactoryInterface $paramsCollectionFactory;

    public function __construct(
        LinkGenerator                                   $linkGenerator,
        IntermediateSiteParamCollectionFactoryInterface $paramsCollectionFactory
    )
    {
        $this->linkGenerator = $linkGenerator;
        $this->paramsCollectionFactory = $paramsCollectionFactory;
    }

    /**
     * @throws Exception
     */
    public function generate(IntermediateSitePage $intermediateSitePage, UserParamsCollection $userParams, string $targetUrl): string
    {
        $linkFormatParams = $this->paramsCollectionFactory->create($intermediateSitePage, $userParams, $targetUrl);

        return $this->linkGenerator->withMiddlewares([
            LinkFormatParamSite::class,
            RedirectPage::class,
            UrlBuilder::class,
        ])->generate('', $linkFormatParams);
    }
}