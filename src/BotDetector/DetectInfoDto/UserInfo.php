<?php

namespace RevPush\ServicesBundle\BotDetector\DetectInfoDto;

class UserInfo
{
    private array $info = [];

    public function __get(string $name)
    {
        return $this->info[$name] ?? null;
    }

    public function __set(string $name, $value)
    {
        $this->info[$name] = $value;
    }

    public function __isset(string $name): bool
    {
        return isset($this->info[$name]);
    }

    public function __unset($name)
    {
        unset($this->info[$name]);
    }
}