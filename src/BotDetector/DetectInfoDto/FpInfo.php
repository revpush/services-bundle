<?php

namespace RevPush\ServicesBundle\BotDetector\DetectInfoDto;

class FpInfo
{
    private ?string $fpCode = '';
    private ?string $fpMessage = '';
    private array $fpComponents = [];

    /**
     * @return string|null
     */
    public function getFpCode(): ?string
    {
        return $this->fpCode;
    }

    /**
     * @param string|null $fpCode
     * @return FpInfo
     */
    public function setFpCode(?string $fpCode): FpInfo
    {
        $this->fpCode = $fpCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFpMessage(): ?string
    {
        return $this->fpMessage;
    }

    /**
     * @param string|null $fpMessage
     * @return FpInfo
     */
    public function setFpMessage(?string $fpMessage): FpInfo
    {
        $this->fpMessage = $fpMessage;
        return $this;
    }

    /**
     * @return array
     */
    public function getFpComponents(): array
    {
        return $this->fpComponents;
    }

    /**
     * @param array $fpComponents
     * @return FpInfo
     */
    public function setFpComponents(array $fpComponents): FpInfo
    {
        $this->fpComponents = $fpComponents;
        return $this;
    }
}