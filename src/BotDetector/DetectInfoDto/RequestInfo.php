<?php

namespace RevPush\ServicesBundle\BotDetector\DetectInfoDto;

class RequestInfo
{
    public const INBOUND_TYPE = 'inbound';
    public const OUTBOUND_TYPE = 'outbound';
    public const UNDEFINED_TYPE = 'undefined';

    private string $type = self::UNDEFINED_TYPE;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return RequestInfo
     */
    public function setType(string $type): RequestInfo
    {
        $this->type = $type;
        return $this;
    }
}