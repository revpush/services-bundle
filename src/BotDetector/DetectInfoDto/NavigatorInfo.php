<?php

namespace RevPush\ServicesBundle\BotDetector\DetectInfoDto;

class NavigatorInfo
{
    private ?bool $webDriver = false;
    private ?string $userAgent = '';
    private ?string $platform = '';

    /**
     * @return bool|null
     */
    public function getWebDriver(): ?bool
    {
        return $this->webDriver;
    }

    /**
     * @param bool|null $webDriver
     * @return NavigatorInfo
     */
    public function setWebDriver(?bool $webDriver): NavigatorInfo
    {
        $this->webDriver = $webDriver;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @param string|null $userAgent
     * @return NavigatorInfo
     */
    public function setUserAgent(?string $userAgent): NavigatorInfo
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    /**
     * @param string|null $platform
     * @return NavigatorInfo
     */
    public function setPlatform(?string $platform): NavigatorInfo
    {
        $this->platform = $platform;
        return $this;
    }
}