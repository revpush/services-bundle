<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class IsCookiesSupportChecker extends AbstractBoolChecker
{
    public static function getGid(): string
    {
        return 'is_cookie_support';
    }

    public function checkIsBot($data): bool
    {
        return !$data;
    }

    public function getReason(): string
    {
        return 'Cookie is not supported';
    }
}