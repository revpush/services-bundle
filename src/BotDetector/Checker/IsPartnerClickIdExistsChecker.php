<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class IsPartnerClickIdExistsChecker extends AbstractStringChecker
{
    public static function getGid(): string
    {
        return 'is_partner_click_id_exists';
    }

    public function getReason(): string
    {
        return 'Partner Click ID is empty';
    }
}