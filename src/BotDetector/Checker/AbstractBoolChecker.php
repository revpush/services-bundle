<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

abstract class AbstractBoolChecker implements CheckerInterface
{
    public function checkIsBot($data): bool
    {
        return (bool) $data;
    }

    public function isValid($data): bool
    {
        return is_bool($data);
    }
}