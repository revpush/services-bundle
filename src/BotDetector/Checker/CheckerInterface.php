<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

interface CheckerInterface
{
    public static function getGid(): string;
    public function checkIsBot($data): bool;
    public function isValid($data): bool;
    public function getReason(): string;
}