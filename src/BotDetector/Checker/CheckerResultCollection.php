<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class CheckerResultCollection implements \IteratorAggregate
{
    /** @var CheckerResult[] */
    private array $items = [];

    public function add(CheckerResult $checkerResult)
    {
        $this->items[$checkerResult->getCheckerName()] = $checkerResult;
    }

    public function get(string $checker): ?CheckerResult
    {
        return $this->items[$checker] ?? null;
    }

    /**
     * @return CheckerResult[]
     */
    public function getAll(): array
    {
        return $this->items;
    }

    /**
     * @return \Traversable
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->items);
    }
}