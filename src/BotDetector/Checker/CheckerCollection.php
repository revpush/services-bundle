<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class CheckerCollection
{
    /** @var CheckerInterface[] */
    private array $items = [];

    public function add(CheckerInterface $checker)
    {
        $this->items[$checker->getGid()] = $checker;
    }

    public function get(string $checkerGid): ?CheckerInterface
    {
        return $this->items[$checkerGid] ?? null;
    }

    /**
     * @return CheckerInterface[]
     */
    public function getAll()
    {
        return $this->items;
    }
}