<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class IsUserAgentExistsChecker extends AbstractStringChecker
{
    public static function getGid(): string
    {
        return 'is_user_agent_exists';
    }

    public function getReason(): string
    {
        return 'User agent is empty';
    }
}