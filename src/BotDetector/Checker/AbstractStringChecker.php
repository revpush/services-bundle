<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

abstract class AbstractStringChecker implements CheckerInterface
{
    public function checkIsBot($data): bool
    {
        return !$data;
    }

    public function isValid($data): bool
    {
        return is_string($data);
    }
}