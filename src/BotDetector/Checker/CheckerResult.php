<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class CheckerResult
{
    private bool $isBot;
    private string $checkerName;
    private string $reason;

    public function __construct(string $checkerName, bool $isBot, string $reason = '')
    {
        $this->checkerName = $checkerName;
        $this->isBot = $isBot;
        $this->reason = $reason;
    }

    public function getCheckerName(): string
    {
        return $this->checkerName;
    }

    public function isBot(): bool
    {
        return $this->isBot;
    }

    public function getReason(): string
    {
        return $this->reason;
    }
}