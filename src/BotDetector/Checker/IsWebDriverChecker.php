<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

class IsWebDriverChecker extends AbstractBoolChecker
{
    public static function getGid(): string
    {
        return 'is_webdriver';
    }

    public function getReason(): string
    {
        return 'This is a webdriver';
    }
}