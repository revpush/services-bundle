<?php

namespace RevPush\ServicesBundle\BotDetector\Checker;

use WhichBrowser\Parser;

class IsUserAgentValidChecker implements CheckerInterface
{
    public static function getGid(): string
    {
        return 'is_user_agent_valid';
    }

    public function checkIsBot($data): bool
    {
        $whichBrowserResult = new Parser($data);

        if (!$whichBrowserResult->isDetected()) {
            return true;
        }

        if ($whichBrowserResult->isType('bot')) {
            return true;
        }

        return false;
    }

    public function isValid($data): bool
    {
        return $data && is_string($data);
    }

    public function getReason(): string
    {
        return 'User Agent is not valid or is bot';
    }
}