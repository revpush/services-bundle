<?php

namespace RevPush\ServicesBundle\BotDetector\Check;

use RevPush\ServicesBundle\BotDetector\Checker\CheckerInterface;

class BotCheck
{
    private CheckerInterface $checker;

    /** @var mixed */
    private $data;

    public function __construct(CheckerInterface $checker, $data)
    {
        $this->checker = $checker;
        $this->data = $data;
    }

    public function checkIsBot(): bool
    {
        return $this->checker->checkIsBot($this->data);
    }

    public function isValid(): bool
    {
        return $this->checker->isValid($this->data);
    }

    public function getReason(): string
    {
        return $this->checker->getReason();
    }

    public function getCheckGid(): string
    {
        return $this->checker->getGid();
    }
}