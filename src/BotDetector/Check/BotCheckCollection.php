<?php

namespace RevPush\ServicesBundle\BotDetector\Check;


class BotCheckCollection
{
    /** @var BotCheck[] */
    private array $checks = [];

    public function add(BotCheck $check): void
    {
        $this->checks[] = $check;
    }

    /**
     * @return BotCheck[]
     */
    public function all(): array
    {
        return $this->checks;
    }
}