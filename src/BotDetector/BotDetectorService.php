<?php

namespace RevPush\ServicesBundle\BotDetector;

use RevPush\ServicesBundle\BotDetector\Check\BotCheck;
use RevPush\ServicesBundle\BotDetector\Check\BotCheckCollection;
use RevPush\ServicesBundle\BotDetector\Definer\DefaultDefiner;
use RevPush\ServicesBundle\BotDetector\Definer\DefinerResult;

class BotDetectorService
{
    private BotCheckerService $botCheckerService;
    private BotDefinerService $botDefinerService;

    public function __construct(
        BotCheckerService $botCheckerService,
        BotDefinerService $botDefinerService
    )
    {
        $this->botCheckerService = $botCheckerService;
        $this->botDefinerService = $botDefinerService;
    }

    public function createChecks(array $checkersAndValues): BotCheckCollection
    {
        $collection = new BotCheckCollection();
        foreach ($checkersAndValues as $checkerName => $value) {
            $checker = $this->botCheckerService->getCheckerByName($checkerName);
            $collection->add(new BotCheck($checker, $value));
        }

        return $collection;
    }

    public function checkAndDefine(BotCheckCollection $checkCollection, string $defineStrategy = DefaultDefiner::class): DefinerResult
    {
        $checkerResultCollection = $this->botCheckerService->check($checkCollection);
        return $this->botDefinerService->define($checkerResultCollection, $defineStrategy);
    }
}