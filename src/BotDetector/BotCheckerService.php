<?php

namespace RevPush\ServicesBundle\BotDetector;


use RevPush\ServicesBundle\BotDetector\Check\BotCheckCollection;
use RevPush\ServicesBundle\BotDetector\Checker\CheckerCollection;
use RevPush\ServicesBundle\BotDetector\Checker\CheckerInterface;
use RevPush\ServicesBundle\BotDetector\Checker\CheckerResult;
use RevPush\ServicesBundle\BotDetector\Checker\CheckerResultCollection;

class BotCheckerService
{
    private CheckerCollection $botCheckerCollection;

    public function __construct(CheckerCollection $botCheckerCollection)
    {
        $this->botCheckerCollection = $botCheckerCollection;
    }

    public function getCheckerByName(string $checkerName): CheckerInterface
    {
        if ($checker = $this->botCheckerCollection->get($checkerName)) {
            return $checker;
        }

        throw new \RuntimeException('Checker with name ' . $checkerName . ' not found');
    }

    public function check(BotCheckCollection $checkCollection, CheckerResultCollection $checkerResultCollection = null): CheckerResultCollection
    {
        if(!$checkerResultCollection) {
            $checkerResultCollection = new CheckerResultCollection();
        }

        foreach ($checkCollection->all() as $check) {
            if ($check->isValid()) {
                $isBot = $check->checkIsBot();
                $reason = $isBot ? $check->getReason() : '';
                $checkerResult = new CheckerResult(
                    $check->getCheckGid(), $isBot, $reason
                );
            } else {
                $checkerResult = new CheckerResult($check->getCheckGid(), true, 'Data is not valid');
            }

            $checkerResultCollection->add($checkerResult);
        }

        return $checkerResultCollection;
    }
}