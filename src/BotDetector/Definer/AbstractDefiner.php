<?php

namespace RevPush\ServicesBundle\BotDetector\Definer;

use RevPush\ServicesBundle\BotDetector\Checker\CheckerResultCollection;

abstract class AbstractDefiner implements DefinerInterface
{
    public function getDefinerName(): string
    {
        return get_class($this);
    }

    public function define(CheckerResultCollection $checkerResultCollection): DefinerResult
    {
        $definerResult = $this->createDefinerResult();
        foreach ($checkerResultCollection->getAll() as $key => $checkerResult) {
            if ($isBot = $checkerResult->isBot()) {
                $definerResult->isBot = $isBot;
                $definerResult->reasonCheckers[$key] = $checkerResult->getCheckerName();
                $definerResult->reasons[$key] = $checkerResult->getReason();
            }
        }

        return $definerResult;
    }

    protected function createDefinerResult(): DefinerResult
    {
        return new DefinerResult();
    }
}