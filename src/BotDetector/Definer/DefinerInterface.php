<?php

namespace RevPush\ServicesBundle\BotDetector\Definer;

use RevPush\ServicesBundle\BotDetector\Checker\CheckerResultCollection;

interface DefinerInterface
{
    public function getDefinerName(): string;
    public function define(CheckerResultCollection $checkerResultCollection): DefinerResult;
}