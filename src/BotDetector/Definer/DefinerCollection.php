<?php

namespace RevPush\ServicesBundle\BotDetector\Definer;

class DefinerCollection
{
    /** @var DefinerInterface[] */
    private array $items = [];

    public function add(DefinerInterface $definer)
    {
        $this->items[$definer->getDefinerName()] = $definer;
    }

    public function get(string $definerName): ?DefinerInterface
    {
        return $this->items[$definerName] ?? null;
    }

    /**
     * @return DefinerInterface[]
     */
    public function getAll()
    {
        return $this->items;
    }
}