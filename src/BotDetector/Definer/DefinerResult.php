<?php

namespace RevPush\ServicesBundle\BotDetector\Definer;

class DefinerResult
{
    public bool $isBot = false;
    public array $reasons = [];
    public array $reasonCheckers = [];
    public string $destinationUrl = 'https://google.com';
}