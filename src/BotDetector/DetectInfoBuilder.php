<?php

namespace RevPush\ServicesBundle\BotDetector;

use RevPush\ServicesBundle\BotDetector\DetectInfoDto\FpInfo;
use RevPush\ServicesBundle\BotDetector\DetectInfoDto\NavigatorInfo;
use RevPush\ServicesBundle\BotDetector\DetectInfoDto\RequestInfo;
use RevPush\ServicesBundle\BotDetector\DetectInfoDto\UserInfo;

class DetectInfoBuilder
{
    public static function buildFromArray(DetectInfo $detectInfo, array $data)
    {
        if(isset($data[DetectInfo::REQUEST_INFO]) && is_array($data[DetectInfo::REQUEST_INFO])) {
            DetectInfoBuilder::buildRequestInfo($detectInfo->getRequestInfo(), $data[DetectInfo::REQUEST_INFO]);
        }

        if(isset($data[DetectInfo::USER_INFO]) && is_array($data[DetectInfo::USER_INFO])) {
            DetectInfoBuilder::buildUserInfo($detectInfo->getUserInfo(), $data[DetectInfo::USER_INFO]);
        }

        if(isset($data[DetectInfo::FP_INFO]) && is_array($data[DetectInfo::FP_INFO])) {
            DetectInfoBuilder::buildFpInfo($detectInfo->getFpInfo(), $data[DetectInfo::FP_INFO]);
        }

        if(isset($data[DetectInfo::NAVIGATOR_INFO]) && is_array($data[DetectInfo::NAVIGATOR_INFO])) {
            DetectInfoBuilder::buildNavigatorInfo($detectInfo->getNavigatorInfo(), $data[DetectInfo::NAVIGATOR_INFO]);
        }
    }

    public static function buildRequestInfo(RequestInfo $requestInfo, array $data)
    {
        foreach($data as $key => $value) {
            self::setIfIssetAndCallable($requestInfo, $key, $value);
        }
    }

    public static function buildUserInfo(UserInfo $userInfo, array $data)
    {
//        foreach($data as $key => $value) {
//            self::setIfIssetAndCallable($userInfo, $key, $value);
//        }
    }

    public static function buildFpInfo(FpInfo $fpInfo, array $data)
    {
        foreach($data as $key => $value) {
            self::setIfIssetAndCallable($fpInfo, $key, $value);
        }
    }

    public static function buildNavigatorInfo(NavigatorInfo $navigatorInfo, array $data)
    {
        foreach($data as $key => $value) {
            self::setIfIssetAndCallable($navigatorInfo, $key, $value);
        }
    }

    public static function setIfIssetAndCallable(object $toObject, string $field, $value): bool
    {
        if(!empty($value)) {
            $method = 'set' . ucfirst($field);
            if(is_callable([$toObject, $method], true)) {
                $toObject->{$method}($value);
                return true;
            }
        }

        return false;
    }
}