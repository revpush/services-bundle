<?php

namespace RevPush\ServicesBundle\BotDetector;

use RevPush\ServicesBundle\BotDetector\Checker\CheckerResultCollection;
use RevPush\ServicesBundle\BotDetector\Definer\DefaultDefiner;
use RevPush\ServicesBundle\BotDetector\Definer\DefinerCollection;
use RevPush\ServicesBundle\BotDetector\Definer\DefinerResult;

class BotDefinerService
{
    private DefinerCollection $definerCollection;

    public function __construct(DefinerCollection $definerCollection)
    {
        $this->definerCollection = $definerCollection;
    }

    public function define(CheckerResultCollection $checkerResultCollection, string $definerName = DefaultDefiner::class): DefinerResult
    {
        if(!$definer = $this->definerCollection->get($definerName)) {
            throw new \Exception('Definer ' . $definerName . ' not found');
        }

        return $definer->define($checkerResultCollection);
    }
}