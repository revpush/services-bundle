<?php

namespace RevPush\ServicesBundle\BotDetector;

use RevPush\ServicesBundle\BotDetector\DetectInfoDto\FpInfo;
use RevPush\ServicesBundle\BotDetector\DetectInfoDto\NavigatorInfo;
use RevPush\ServicesBundle\BotDetector\DetectInfoDto\RequestInfo;
use RevPush\ServicesBundle\BotDetector\DetectInfoDto\UserInfo;

class DetectInfo
{
    private RequestInfo $requestInfo;
    private FpInfo $fpInfo;
    private UserInfo $userInfo;
    private NavigatorInfo $navigatorInfo;

    const REQUEST_INFO = 'requestInfo';
    const USER_INFO = 'userInfo';
    const FP_INFO = 'fpInfo';
    const NAVIGATOR_INFO = 'navigatorInfo';

    public function __construct()
    {
        $this->requestInfo = new RequestInfo();
        $this->fpInfo = new FpInfo();
        $this->userInfo = new UserInfo();
        $this->navigatorInfo = new NavigatorInfo();
    }

    /**
     * @return RequestInfo
     */
    public function getRequestInfo(): RequestInfo
    {
        return $this->requestInfo;
    }

    /**
     * @param RequestInfo $requestInfo
     * @return DetectInfo
     */
    public function setRequestInfo(RequestInfo $requestInfo): DetectInfo
    {
        $this->requestInfo = $requestInfo;
        return $this;
    }

    /**
     * @return FpInfo
     */
    public function getFpInfo(): FpInfo
    {
        return $this->fpInfo;
    }

    /**
     * @param FpInfo $fpInfo
     * @return DetectInfo
     */
    public function setFpInfo(FpInfo $fpInfo): DetectInfo
    {
        $this->fpInfo = $fpInfo;
        return $this;
    }

    /**
     * @return UserInfo
     */
    public function getUserInfo(): UserInfo
    {
        return $this->userInfo;
    }

    /**
     * @param UserInfo $userInfo
     * @return DetectInfo
     */
    public function setUserInfo(UserInfo $userInfo): DetectInfo
    {
        $this->userInfo = $userInfo;
        return $this;
    }

    /**
     * @return NavigatorInfo
     */
    public function getNavigatorInfo(): NavigatorInfo
    {
        return $this->navigatorInfo;
    }

    /**
     * @param NavigatorInfo $navigatorInfo
     * @return DetectInfo
     */
    public function setNavigatorInfo(NavigatorInfo $navigatorInfo): DetectInfo
    {
        $this->navigatorInfo = $navigatorInfo;
        return $this;
    }
}