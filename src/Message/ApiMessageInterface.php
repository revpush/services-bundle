<?php

namespace RevPush\ServicesBundle\Message;

interface ApiMessageInterface
{
    public function getClass(): string;
}